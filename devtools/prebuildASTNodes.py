import keyword
import os
from pathlib import Path
from typing import Dict, List, Tuple

import autopep8
import isort
from buildtools.indentation import IndentWriter
from ruamel.yaml import YAML as Yaml

YAML = Yaml(typ='rt', pure=True)


def main():
    print('Rebuilding nodes...', end='')
    nodes: Dict[str, Tuple[str, List[str]]]
    with open('data/nodes.yml', 'r') as f:
        nodes = YAML.load(f)
    OUTFILE = Path('mwi') / 'php' / '_basetree.py'
    TMPFILE = OUTFILE.with_suffix('.py~')
    num_nodes = 0
    with TMPFILE.open('w') as f:
        w = IndentWriter(f, indent_chars=(' '*4))
        w.writeline('from typing import Any,List')
        w.writeline('from mwi.php.node import PHPNode')
        l = sorted(list(map(lambda x: f'_BasePHP{x}', nodes.keys())))
        w.writeline(f'__all__ = {l!r}')
        for key, v in nodes.items():
            name, fields = v
            with w.writeline(f'class _BasePHP{key}(PHPNode):'):
                args = []
                argnames = []
                for fieldname in fields:
                    if keyword.iskeyword(fieldname):
                        fieldname += '_'
                    args.append(f'{fieldname}: Any')
                    argnames.append(fieldname)
                argsstr = ', '.join(args)
                argnamesstr = ', '.join(argnames)
                w.writeline(f'FIELDS: List[str] = {argnames!r}')
                with w.writeline(f'def __init__(self, {argsstr}, **kwargs) -> None:'):
                    w.writeline(f'super().__init__({argnamesstr}, **kwargs)')
                    for i, fieldname in enumerate(argnames):
                        w.writeline(f'self.{fieldname} = {fieldname}')
            num_nodes += 1
    code = TMPFILE.read_text()
    code = isort.code(code)
    code = autopep8.fix_code(code)
    TMPFILE.write_text(code)
    os.replace(TMPFILE, OUTFILE)
    print(' Done.')
    print(f'Output {num_nodes} classes to {OUTFILE}.')


if __name__ == "__main__":
    main()

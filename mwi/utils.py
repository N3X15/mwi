
from typing import Any, Dict, List, Set


def merge_dict(source, destination):
    """
    run me with nosetests --with-doctest file.py

    >>> a = { 'first' : { 'all_rows' : { 'pass' : 'dog', 'number' : '1' } } }
    >>> b = { 'first' : { 'all_rows' : { 'fail' : 'cat', 'number' : '5' } } }
    >>> merge(b, a) == { 'first' : { 'all_rows' : { 'pass' : 'dog', 'fail' : 'cat', 'number' : '5' } } }
    True
    """
    o = {}
    for k in set(list(source.keys()) + list(destination.keys())):
        if k in source:
            srcval = source[k]
            if isinstance(srcval, dict):
                if k not in destination or not isinstance(destination[k], dict):
                    o[k] = srcval
                else:
                    o[k] = merge_dict(srcval, destination[k])
            else:
                o[k] = srcval
        elif k in destination:
            o[k] = destination[k]
    return o


class DeleteMe(object):
    pass


def diff_dict(a: dict, b: dict) -> dict:
    o = {}
    for k in set(list(a.keys()) + list(b.keys())):
        if k in a and k in b:
            va = a[k]
            vb = b[k]
            if isinstance(va, dict) and isinstance(vb, dict):
                diff = diff_dict(va, vb)
                if len(diff.keys()) > 0:
                    o[k] = diff
            #elif isinstance(va, list) and isinstance(vb, list):
            #    o[k] = vb
            elif va != vb:
                o[k] = vb
        elif k in b:
            o[k] = b[k]
    return o


class PHPRawCode:
    def __init__(self, code: str) -> None:
        self.code = code

    def __str__(self) -> str:
        return self.code


class PyValue2PHPVisitor:
    def __init__(self) -> None:
        pass

    def handle(self, thing: Any) -> str:
        if thing is None:
            return 'null'
        match type(thing):
            case str():
                return self.handle_str(thing)
            case int():
                return self.handle_int(thing)
            case float():
                return self.handle_float(thing)
            case list():
                return self.handle_list(thing)
            case set():
                return self.handle_set(thing)
            case dict():
                return self.handle_dict(thing)
            case _ if isinstance(thing, PHPRawCode):
                return thing.code
            case _:
                return '/*Unable to handle type '+type(thing)+'*/'

    def handle_str(self, s: str) -> str:
        o: str = "'"

        def esc(_s: str) -> str:
            return f'\\{_s}'
        for c in s:
            match c:
                case "'":
                    o += esc("'")
                case '\n':
                    o += esc("n")
                case '\r':
                    o += esc("r")
                case '\t':
                    o += esc("t")
                case '\x0B':
                    o += esc("v")
                case '\x1B':
                    o += esc("e")
                case '\x0C':
                    o += esc("f")
                case '\\':
                    o += esc("\\")
                case _ if ord(c) < 127:
                    o += c
                case _:
                    o += f'\u{{{ord(c):04X}}}'
        return o + "'"

    def handle_int(self, i: int) -> str:
        return repr(i)

    def handle_float(self, f: float) -> str:
        return str(f)

    def handle_list(self, l: List[Any]) -> str:
        return '[{0}]'.format(', '.join([self.handle(x) for x in l]))

    def handle_set(self, s: Set[Any]) -> str:
        return '[{0}]'.format(', '.join([self.handle(x) for x in sorted(s)]))

    def handle_dict(self, d: Dict[Any, Any]) -> str:
        return '[{0}]'.format(', '.join([f'{self.handle(k)} => {self.handle(v)}' for k, v in d.items()]))

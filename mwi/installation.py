from __future__ import annotations
from pathlib import Path
import string
from threading import local
from typing import Any, Dict, List, Optional
from mwi.extension import MWExtension

from mwi.old_config import OldMWConfig
from mwi.utils import PyValue2PHPVisitor


class MWInstallation:
    def __init__(self, id: Optional[str] = None, path: Optional[Path] = None, local_settings: Optional[Dict[str, Any]] = None,
                 plugins: Optional[Dict[str, MWExtension]] = None, skins: Optional[Dict[str, MWExtension]] = None):
        self.id: Optional[str] = id
        self.path: Optional[Path] = path
        self.local_settings: Dict[str, Any] = local_settings or {}
        self.plugins: Dict[str, MWExtension] = plugins or {}
        self.skins: Dict[str, MWExtension] = skins or {}

    def generateLocalSettings(self) -> str:
        ls = dict(self.local_settings)
        for plug in self.plugins.values():
            ls.update(plug.addLocalSettings(self))
        for skin in self.skins.values():
            ls.update(skin.addLocalSettings(self))
        v2p = PyValue2PHPVisitor()
        o: str = '<?php /* Protect against web entry */\n'
        o += 'if ( !defined(\'MEDIAWIKI\') ) { exit; }\n'
        o += '/*****************************************\n'
        o += ' * !!!!!!!!!!!! DO NOT EDIT !!!!!!!!!!!! *\n'
        o += ' * This file is automatically generated! *\n'
        o += ' *****************************************/\n\n'
        o += '/* @mwi: start variables */\n'
        for k, v in sorted(ls.items()):
            o += f'${k} = {v2p.handle(v)};\n'
        o += '/* @mwi: end variables */\n\n'
        o += '/* @mwi: start extensions */\n'
        for k, x in sorted(self.plugins.items()):
            o += x.generateInclude()
        o += '/* @mwi: end extensions */\n\n'
        o += '/* @mwi: start skins */\n'
        for k, x in sorted(self.plugins.items()):
            o += x.generateInclude()
        o += '/* @mwi: end skins */\n\n'
        return o

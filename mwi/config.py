import os, typing, re, collections, json, shutil
from typing import Optional, Any
from buildtools.config import BaseConfig, YAMLConfig
from buildtools import os_utils, log
from buildtools.indentation import IndentWriter
from buildtools.utils import sha256sum

from mwi.phpstuff import php_var_export, php_gen_dict_sets, php_gen_list_sets
from mwi.extension import MWExtensionLibrary
from mwi.constants import MWI_DIR
from mwi.utils import merge_dict, diff_dict

REG_INCLUDE_NO_COMMAS=re.compile(r'(require|require_once|include)\s+("[^"]+");')
class MWConfig(object):
    Instances = {}
    MW_STOCK_VARS=None

    def __init__(self, _id: str, globalconf: YAMLConfig, script_dir: str, write_only: bool = False):
        if self.MW_STOCK_VARS is None:
            stockvarsfile = os.path.join(MWI_DIR, 'data', 'vars.json')
            #print(f'Reading {stockvarsfile}')
            with open(stockvarsfile, 'r') as f:
                self.MW_STOCK_VARS = json.load(f, object_pairs_hook=collections.OrderedDict)

        self.ID = _id
        self.mwi_path = MWI_DIR
        self.path = globalconf.get('filesystem.pathtemplate').format(HOST=_id)
        self.p2j_path = os.path.join(script_dir, 'vars2json.php')
        self.cfgpath = os.path.join(self.path, '.mwi-config.yml')
        self.p2j_config_path = os.path.join(script_dir, '.p2j-config.json')
        self.conf_d_dir = os.path.join(self.path, 'conf.d')
        self.config = None
        self.script_dir = script_dir

        import_from_php = False
        if os.path.isfile(self.cfgpath):
            self.config = YAMLConfig(self.cfgpath, ordered_dicts=True)
        else:
            self.config = YAMLConfig(None, ordered_dicts=True)
            import_from_php = False

        self.variables          = self.config.get('variables', self.config.get('localconfig', {}))
        if 'localconfig' in self.variables.keys():
            del self.variables['localconfig']
        if 'variables' in self.variables.keys():
            del self.variables['variables']
        skins                   = self.uniqueInList(self.config.get('skins',              []))
        extensions              = self.uniqueInList(globalconf.get('extensions.required',[])+self.config.get('extensions',[]))
        self.mysql_username     = self.config.get('mysql.user',         None)
        self.mysql_password     = self.config.get('mysql.pass',         None)
        self.admin_username     = self.config.get('initial-admin.user', None)
        self.admin_password     = self.config.get('initial-admin.pass', None)
        self.localsettings_hash = self.config.get('php_checksum',       None)

        for oldID, newID in globalconf.get('extensions.replace',{}).items():
            if oldID in extensions:
                extensions.remove(oldID)
                if newID not in extensions:
                    extensions.append(newID)

        self.skins=[]
        for skinID in skins:
            mwext = MWExtensionLibrary.GetByID(skinID)
            self.skins += [mwext]

        self.extensions=[]
        for extID in extensions:
            mwext = MWExtensionLibrary.GetByID(extID)
            self.extensions += [mwext]

        if self.localsettings_hash is None and not write_only:
            self.importLocalSettings()

    @staticmethod
    def Get(_id: str, globalconf: YAMLConfig, script_dir: str, for_completion: bool = False):
        if _id not in MWConfig.Instances.keys():
            MWConfig.Instances[_id] = MWConfig(_id, globalconf, script_dir, write_only=for_completion)
        return MWConfig.Instances[_id]

    def uniqueInList(self, _input: list) -> list:
        output = []
        for e in _input:
            if e not in output:
                output.append(e)
        return output

    def save(self):
        # (self\.[a-z_]+)\s*= self\.config\.get\(('[^']+'),.*$
        # self.config.set($2, $1)
        #self.config.cfg=collections.OrderedDict()
        self.config.set('variables', self.variables)
        self.config.set('mysql.user', self.mysql_username)
        self.config.set('mysql.pass', self.mysql_password)
        self.config.set('initial-admin.user', self.admin_username)
        self.config.set('initial-admin.pass', self.admin_password)
        self.config.set('skins', self.uniqueInList([mwext.ID for mwext in self.skins]))
        self.config.set('extensions', self.uniqueInList([mwext.ID for mwext in self.extensions]))
        self.config.set('php_checksum', self.localsettings_hash)
        self.config.Save(self.cfgpath)

    def importLocalSettings(self, localsettings_path=None):
        self.variables=collections.OrderedDict()
        log.info('Parsing LocalSettings...')

        if localsettings_path is None:
            localsettings_path = os.path.join(self.path, 'LocalSettings.php')

        self.localsettings_hash = sha256sum(localsettings_path)

        os_utils.cmd(['php', self.p2j_path, localsettings_path, self.p2j_config_path], critical=True, echo=True, show_output=True)

        with open(self.p2j_config_path, 'r') as f:
            self.variables=json.load(f, object_pairs_hook=collections.OrderedDict)

        os.remove(self.p2j_config_path)

        loaded=set()
        self.skins      = []
        if '_mwiSkins' in self.variables:
            for extDir in self.variables['_mwiSkins']:
                mwext = MWExtensionLibrary.GetByLoaderArg(extDir)
                if mwext.ID not in loaded:
                    loaded.add(mwext.ID)
                    if mwext.type == 'skin':
                        self.skins += [mwext]
                    else:
                        self.extensions += [mwext]
            del self.variables['_mwiSkins']

        self.extensions = []
        if '_mwiExtensions' in self.variables:
            for extDir in self.variables['_mwiExtensions']:
                mwext = MWExtensionLibrary.GetByLoaderArg(extDir)
                if mwext.ID not in loaded:
                    loaded.add(mwext.ID)
                    if mwext.type == 'skin':
                        self.skins += [mwext]
                    else:
                        self.extensions += [mwext]
            del self.variables['_mwiExtensions']


        if '_mwiIncludes' in self.variables:
            del self.variables['_mwiIncludes']
        #del self.variables['localconfig']

        self.variables = diff_dict(self.MW_STOCK_VARS, self.variables)

        if 'localconfig' in self.variables.keys():
            del self.variables['localconfig']
        if 'variables' in self.variables.keys():
            del self.variables['variables']

        log.info('Loaded %d variables, %d skins, and %d extensions from LocalSettings.php...', len(self.variables.keys()), len(self.skins), len(self.extensions))

    def cleanup(self):
        litter = [
            'LocalSettings.php.new', # Old, vulnerable.
            'LocalSettings.new.php', # Old proposed file.
            'LocalSettings.tmp.php', # Temporary file fragment.
        ]
        for basename in litter:
            oldnewfile = os.path.join(self.path, basename)
            if os.path.isfile(oldnewfile):
                log.info('Removing %s...', oldnewfile)
                os.remove(oldnewfile)

    def defined(self, varname: str) -> bool:
        return varname in self.variables.keys()

    def calculateSettings(self):
        # $wgServer added in 1.34, mandatory.
        if not self.defined('wgServer'):
            self.variables['wgServer'] = f'https://{self.ID}'
            log.warning('$wgServer added, as it was not present. Value = %r', self.variables['wgServer'])

    def saveLocalSettings(self, localsettings_path=None, pretty_print=False):
        self.calculateSettings()
        if localsettings_path is None:
            localsettings_path = os.path.join(self.path, 'LocalSettings.php')
            backup=False
            if os.path.isfile(localsettings_path):
                with open(localsettings_path, 'r') as f:
                    if '@generated by mwi.py' not in f.read():
                        if not os.path.isfile(os.path.join(self.path, 'LocalSettings.backup.php')):
                            backup=True
            if backup:
                with log.info('Backing up LocalSettings.php -> LocalSettings.backup.php...'):
                    os_utils.single_copy(
                        os.path.join(self.path, 'LocalSettings.php'),
                        os.path.join(self.path, 'LocalSettings.backup.php')
                    )
        destdir = os.path.dirname(localsettings_path)
        basename, _ = os.path.splitext(os.path.basename(localsettings_path))
        tmpfn = os.path.join(destdir, basename+'.tmp.php')
        with log.info('Writing %s...', tmpfn):
            lines = []
            with open(os.path.join(self.mwi_path, 'src', 'templates', 'LocalSettings.php'), 'r') as f:
                for line in f:
                    lines += [line.rstrip()]

            with open(tmpfn, 'w') as f:
                w = IndentWriter(f)

                for line in lines:
                    w.writeline(line)

                w.writeline()

                DEBUG_VARS = self.config.get('debug.variables', False)

                localsettings_vars = dict(self.MW_STOCK_VARS)

                if 'localconfig' in self.variables.keys():
                    del self.variables['localconfig']
                if 'localconfig' in localsettings_vars.keys():
                    del localsettings_vars['localconfig']
                if 'variables' in self.variables.keys():
                    del self.variables['variables']
                if 'variables' in localsettings_vars.keys():
                    del localsettings_vars['variables']

                localsettings_vars = merge_dict(self.variables, localsettings_vars)

                w.writeline('/* @mwi: start variables */')
                for key in sorted(localsettings_vars.keys()):
                    value = localsettings_vars[key]
                    if key in ('localconfig', 'variables'):
                        continue
                    if key in self.MW_STOCK_VARS.keys():
                        if isinstance(value, (dict, collections.OrderedDict)):
                            for difference in sorted(php_gen_dict_sets(key, self.MW_STOCK_VARS[key], value)):
                                w.writeline(difference)
                        elif isinstance(value, list):
                            for difference in sorted(php_gen_list_sets(key, self.MW_STOCK_VARS[key], value)):
                                w.writeline(difference)
                        else:
                            if self.MW_STOCK_VARS[key] != value:
                                value=php_var_export(value, pretty_print=pretty_print, prefix=' '*(1+len(key)+3))
                                if DEBUG_VARS:
                                    w.writeline(f'${key} = {value}; // DIFFERS')
                                else:
                                    w.writeline(f'${key} = {value};')
                    else:
                        value=php_var_export(value, pretty_print=pretty_print, prefix=' '*(1+len(key)+3))
                        if DEBUG_VARS:
                            w.writeline(f'${key} = {value}; // NOT IN MW_STOCK_VARS')
                        else:
                            w.writeline(f'${key} = {value};')
                w.writeline('/* @mwi: end variables */')
                w.writeline()
                w.writeline('/* @mwi: start extensions */')
                for ext in self.extensions:
                    #self.writeExtension(w, extID)
                    ext.writeExtension(w, self)
                w.writeline('/* @mwi: end extensions */')
                w.writeline()
                w.writeline('/* @mwi: start skins */')
                for ext in self.skins:
                    #self.writeExtension(w, extID)
                    ext.writeExtension(w, self)
                w.writeline('/* @mwi: end skins */')
        if os.path.isfile(localsettings_path):
            os.remove(localsettings_path)
        shutil.move(tmpfn, localsettings_path)

    def writeExtension(self, w, extID):
        settings = self.library[extID]
        prefix = ''
        code = settings.get('variables', {})
        exttype = settings.get('type', 'extension')
        loadmethod = ''
        loadarg = settings.get('load_args', [extID])
        reposubdir = ''
        if exttype == 'extension':
            reposubdir = 'extensions'
            loadmethod = 'wfLoadExtension'
            if settings.get('legacy-loader', False):
                #loadmethod = 'require_once'
                loadmethod = 'mwiRequireOnce'
                loadarg = [f'$IP/extensions/{extID}/{extID}.php']
        if exttype == 'skin':
            repodir = 'skins'
            loadmethod = 'wfLoadSkin'
            if settings.get('legacy-loader', False):
                #loadmethod = 'require_once'
                loadmethod = 'mwiRequireOnce'
                loadarg = [f'$IP/skins/{extID}/{extID}.php']

        repodir = os.path.join(reposubdir, extID)

        code = settings.get('code', [])
        suffix = ''
        if len(code) > 0:
            os_utils.ensureDirExists(self.conf_d_dir, noisy=True)
            loaded_config_file = config_file = os.path.join(self.conf_d_dir, extID+'.settings.php')
            if os.path.isfile(config_file):
                config_file = os.path.join(self.conf_d_dir, extID+'.settings.new.php')
            with open(config_file, 'w') as cf:
                cw = IndentWriter(cf)
                cw.writeline('<?php')
                cw.writeline('// Protect against web entry')
                cw.writeline("if ( !defined('MEDIAWIKI') ) { exit; }")
                cw.writeline()
                #cf.write('{}({});\n\n'.format(loadmethod, ', '.join(loadarg)))
                for line in code:
                    line = line.replace('{{REPO_DIR}}', repodir)
                    cf.write('{}\n'.format(line))
            suffix = f" mwiRequireOnce('{loaded_config_file}');"
        loadargs=', '.join(loadarg)
        w.writeline(f"/* @mwi: extension {extID} */ {prefix}{loadmethod}({loadargs});")

import collections
from typing import Any

DEBUG_PHP = False


def php_var_export(value: Any, escape_vars: bool = True, path: list = [], pretty_print: bool = False, indent_char: str = '    ', level: int = 0, prefix: str = '') -> str:
    # Prefix
    p = ''
    # Newline
    nl = ''
    # Next-level Prefix
    np = ''
    if pretty_print:
        nl = '\n'
        p = prefix + (indent_char * level)
        np = prefix + (indent_char * (level + 1))
    path.append(value)
    if value is None:
        return 'null'
    elif isinstance(value, str):
        value = value.replace('\\', '\\\\')
        if '$' in value and not escape_vars:
            value = value.replace('"', '\\"')
            return f'"{value}"'
        else:
            value = value.replace("'", "\\'")
            return f"'{value}'"
    elif isinstance(value, (int, float)):
        return str(value)
    elif isinstance(value, bool):
        return 'true' if value else 'false'
    elif isinstance(value, list):
        return '[' + nl + np+(', ' + nl+np).join([php_var_export(x, escape_vars, path, pretty_print, indent_char, level+1, prefix) for x in value])+nl + p + ']'
    elif isinstance(value, (dict, collections.OrderedDict)):
        return '[' + nl + np+(', ' + nl+np).join([php_var_export(k, escape_vars, path, pretty_print, indent_char, level+1, prefix) + ' => ' + php_var_export(value[k], escape_vars, path, pretty_print, indent_char, level+1, prefix) for k in sorted(value.keys())])+nl + p + ']'
    return '/* UNKNOWN: {!r} */ null'.format(value)


def php_gen_list_sets(varName: str, orig: list, current: list, path: list = [], pretty_print: bool = False, indent_char: str = '    ', level: int = 0, prefix: str = '') -> list:
    o = orig
    c = current
    diffs = []
    # print(repr(o))
    '''
    for pe in path:
        if isinstance(o, dict):
            o=o.get(pe,[])
            c=c.get(pe,[])
        else:
            o=o[pe]
            c=c[pe]
    '''
    if not isinstance(o, list) or not isinstance(c, list):
        keyfix = '$' + varName + \
            ''.join(["[" + php_var_export(k) + "]" for k in path])
        value = php_var_export(c, pretty_print=pretty_print, indent_char=indent_char, level=level, prefix=prefix)
        if DEBUG_PHP:
            tc = type(c)
            to = type(o)
            return [f'{keyfix} = {value}; //c:{tc},o:{to}']
        else:
            return [f'{keyfix} = {value};']

    keyfix = '$' + varName + \
        ''.join(["[" + php_var_export(k) + "]" for k in path])
    # so=set(o)
    # sc=set(c)
    for entry in c:
        if entry not in o:
            diffs += [f"{keyfix}[] = " + php_var_export(entry, pretty_print=pretty_print, indent_char=indent_char, level=level, prefix=prefix) + ';']
    for entry in o:
        if entry not in c:
            xe = php_var_export(entry)
            diffs += [f"if (\\in_array({xe}, {keyfix})) {keyfix} = \\array_splice({keyfix}, array_search({xe}, {keyfix}, true), 1);"]
    return diffs


def php_gen_dict_sets(varName: str, orig: dict, current: dict, path: list = [], pretty_print: bool = False, indent_char: str = '    ', level: int = 0, prefix: str = '') -> list:
    o = orig
    c = current
    diffs = []
    '''
    for pe in path:
        if isinstance(pe, str):
            o=o.get(pe,{})
            c=c.get(pe,{})
        else:
            o=o[pe]
            c=c[pe]
    '''
    if not isinstance(o, (dict, collections.OrderedDict)) or not isinstance(c, (dict, collections.OrderedDict)):
        keyfix = '$' + varName + \
            ''.join(['[' + php_var_export(k) + ']' for k in path])
        value = php_var_export(c, pretty_print=pretty_print, indent_char=indent_char, level=level+1, prefix=prefix)
        if DEBUG_PHP:
            tc = type(c)
            to = type(o)
            return [f'{keyfix} = {value}; //c:{tc},o:{to}']
        else:
            return [f'{keyfix} = {value};']
    keys = set(list(o.keys()) + list(c.keys()))
    for key in keys:
        keyfix = '$' + varName + \
            ''.join(['[' + php_var_export(k) + ']' for k in path + [key]])
        if key in o and key not in c:
            diffs += [f'delete {keyfix};']
        elif key not in o and key in c:
            value = php_var_export(c[key], pretty_print=pretty_print, indent_char=indent_char, level=level+1, prefix=prefix)
            diffs += [f'{keyfix} = {value};' + ('// new' if DEBUG_PHP else '')]
        elif key in o and key in c:
            if isinstance(c[key], (dict, collections.OrderedDict)) != isinstance(o[key], (dict, collections.OrderedDict)):
                value = php_var_export(c[key], pretty_print=pretty_print, indent_char=indent_char, level=level+1, prefix=prefix)
                if DEBUG_PHP:
                    tc = type(c[key])
                    to = type(o[key])
                    diffs += [f'{keyfix} = {value}; //c:{tc},to:{to}']
                else:
                    diffs += [f'{keyfix} = {value};']
            else:
                if isinstance(c[key], (dict, collections.OrderedDict)) and isinstance(o[key], (dict, collections.OrderedDict)):
                    diffs += php_gen_dict_sets(varName,
                                               o[key], c[key], path + [key], pretty_print=pretty_print, indent_char=indent_char, level=level+1, prefix=prefix)
                elif isinstance(c[key], list) and isinstance(o[key], list):
                    '''
                    olen = len(o[key])
                    clen = len(c[key])
                    just_remake_it=False
                    diffbuff=[]
                    if olen <= clen:
                        for i in range(max(clen, olen)):
                            if i < clen and i < olen:
                                if o[key][i] != c[key][i]:
                                    just_remake_it = True
                                    break
                            elif i < clen and i >= olen:
                                value=php_var_export(c[key])
                                diffbuff += [f'{keyfix}[] = {value};']
                    if just_remake_it:
                        value=php_var_export(c[key])
                        if DEBUG_PHP:
                            diffs += [f'{keyfix} = {value}; // just_remake_it']
                        else:
                            diffs += [f'{keyfix} = {value};']
                    else:
                        diffs += diffbuff
                    '''
                    diffs += php_gen_list_sets(varName,
                                               o[key], c[key], path + [key], pretty_print=pretty_print, indent_char=indent_char, level=level+1, prefix=prefix)
                else:
                    if c[key] != o[key]:
                        value = php_var_export(c[key])
                        if DEBUG_PHP:
                            tc = type(c[key])
                            to = type(o[key])
                            diffs += [f'{keyfix} = {value}; // c:{tc},o:{to}']
                        else:
                            diffs += [f'{keyfix} = {value};']
    return diffs

from typing_extensions import Self
from logging import getLogger
import sys
from typing import TYPE_CHECKING, Any, Dict, List, Optional, Type, ModuleType
import importlib

from buildtools.bt_logging import IndentLogger
log = IndentLogger(getLogger(__name__))

if TYPE_CHECKING:
    # pymysql is currently working on Kubuntu, so we'll use it for typechecking.
    import pymysql
    import pymysql.cursors

class DBMSConnection:
    KNOWN_MODULES: List[str] = [
        'MYSQLdb',
        '_mysql',
        'pymysql',
        'PyMySQL',
    ]
    def __init__(self, mysqlconf: Dict[str, Any]) -> None:
        self.host: str = mysqlconf['host']
        self.user: str = mysqlconf['maint']['user']
        self.password: str = mysqlconf['maint']['pass']
        
        self.mysqldb: Optional[ModuleType['pymysql']] = None

        self.typeof_cursor: Optional[Type['pymysql.cursors.Cursor']]
        
        self.db: Optional['pymysql.Connection'] = None
        self.cursor: Optional['pymysql.cursors.Cursor'] = None

    def connect(self) -> None:
        if self.db is None:
            self.__doImport()
            log.info('Connecting to %s as %s...', self.host, self.user)
            self.db = self.mysqldb.connect(self.host, self.user, self.password)

    def __importModule(self, modname, package: Optional[str]) -> Any:
        return importlib.import_module(modname, package)

    def __enter__(self) -> Self:
        if self.db is None:
            self.connect()

    def tryToImport(self, modname) -> bool:
        print(f'Checking for module {modname}... ', end='')
        if modname not in sys.modules.keys():
            print('Missing')
            return False
        print('Present!')
        
        print(f'Attempting `import {modname}`... ', end='')
        try:
            self.mysqldb = self.__importModule(modname)
            print('OK!')
            return True
        except:
            print('FAILED!')
        return False

    def __doImport(self) -> None:
        # Because there's 5 different MySQL libraries and only half of them are working at a time...
        for modname in self.KNOWN_MODULES:
            if self.tryToImport(modname):
                return
from __future__ import annotations
from typing import Any, List, Dict, Optional, Set

class ConfigNode:
    def __init__(self) -> None:
        self.children: List[ConfigNode] = []

class ConstantNode(ConfigNode):
    def __init__(self, value: str) -> None:
        super().__init__()
        self.value: str = value

    def __str__(self) -> str:
        return self.value

class IntNode(ConstantNode):
    def __init__(self, value: int) -> None:
        super().__init__(str(value))


class FloatNode(ConstantNode):
    def __init__(self, value: float) -> None:
        super().__init__(str(value))

class AssignmentNode(ConfigNode):
    def __init__(self, varName: str, value: ConfigNode) -> None:
        super().__init__()
        self.varName: str = varName
        self.value: ConfigNode = value
        self.children.append(self.value)
    
    def __str__(self) -> str:
        return f'{self.varName} = {self.value};'
class Block(ConfigNode):
    def __init__(self, parent: Optional[Scope], children: List[ConfigNode] = []) -> None:
        super().__init__()
        self.children = []
        self.scope = parent.allocateChildScope()

class IfNode(ConfigNode):
    def __init__(self, parent: Optional[Scope], expr: str, thenblock: List[ConfigNode], elseblock: List[ConfigNode]) -> None:
        super().__init__()
        self.parent = parent
        self.expr = expr
        self.thenblock = Block(parent, thenblock)
        self.elseblock = Block(parent, elseblock)
    def __str__(self) -> str:
        thenblock = ''.join(filter(lambda x: str(x)+';', self.thenblock))
        elseblock = ''.join(filter(lambda x: str(x)+';', self.elseblock))

        return f'if ({self.expr}) {{{thenblock}}}'+(f' else {{{elseblock}}}' if len(self.elseblock) > 0 else '')
class Scope:
    def __init__(self, parent: Optional[Scope] = None) -> None:
        self.parent = parent
        self.values: Dict[str, ConfigNode] = {}
        self.children: Set[Scope] = set()

    def set(self, var: str, node: ConfigNode) -> None:
        self.values[var] = node
        (x.set(var, node) for x in self.children)

    def unset(self, var: str) -> None:
        del self.values[var]
        (x.unset(var) for x in self.children)

    def allocateChildScope(self) -> Scope:
        s = Scope(self)
        self.children.add(s)
        return s



class MWConfig:
    def __init__(self) -> None:
        self.contents: List[ConfigNode] = {}

    def setVariableStr(self, var: str, val: str) -> None:
        self.contents: List[ConfigNode]
from __future__ import annotations
import os, json
from pathlib import Path
from typing import Any, Dict, List, Optional

from ruamel.yaml import YAML

#from mwi.installation import MWInstallation
yaml = YAML(typ='rt')

from buildtools import os_utils, log
from mwi.constants import MWI_DIR
from mwi.phpstuff import php_var_export

class MWExtensionLoadConfig:
    def __init__(self, extension: 'MWExtension') -> None:
        self.extension: 'MWExtension' = extension
        self.method: str = 'wfLoadExtension'
        self.args: List[Any] = []
        self.load_as: str = ''
        self.from_path: Optional[Path] = None
        self.deprecated: List[Any] = [] # Old load paths

    def deserialize(self, data: Dict[str, Any]) -> None:
        if data is None:
            data={}
        self.load_as = data.get('as', self.extension.ID)
        self.method = 'mwiRequireOnce'
        self.from_path = data.get('from')
        if data.get('legacy', False):
            self.args=data.get('args', [data.get('from', '/'.join(['$IP',self.extension.repo_path,self.load_as+'.php']))])
        else:
            self.method = 'wfLoadSkin' if self.extension.type == 'skin' else 'wfLoadExtension'
            args = [self.load_as]
            if data.get('from'):
                args += [data.get('from')]
            self.args=data.get('args', args)
        self.deprecated=data.get('deprecated', [])

class MWExtension:
    def __init__(self):
        self.ID: str = ''
        self.code: str = ''
        self.variables: Dict[str, Any]={}
        self.load: MWExtensionLoadConfig = MWExtensionLoadConfig(self)
        self.install_by_default: bool = False
        self.type: str = 'extension'
        self.subdir: str = '/'
        self.git_name: str = ''
        self.git_repo: str = ''
        self.repo_path: str = ''
        self.chmod: Dict[str, Any] = {}

        self.variables = {}
        self.variable_values = {}

    def deserialize(self, _id: str, data: Dict[Any, str]):
        self.ID = _id
        self.type = data.get('type', 'extension')
        self.variables = data.get('variables', {})
        self.install_by_default = data.get('install-by-default', False)
        self.chmod = data.get('chmod', {})
        self.git_name = data.get('git-name', _id)
        self.git_repo = data.get('git-repo', f'https://gerrit.wikimedia.org/r/mediawiki/{self.type}s/{self.git_name}.git')
        self.repo_path = data.get('repo_path', '/'.join([self.type+'s', self.ID]))
        self.variables = data.get('variables', {})

        self.load.deserialize(data.get('load') if data else None)

        self.code = []
        pkgfile = MWI_DIR / 'packages' / self.ID / 'settings.php'
        if pkgfile.is_file():
            with pkgfile.open('r') as f:
                for line in f:
                    self.code += [line.rstrip()]

    def writeExtension(self, installation: 'MWInstallation') -> str:
        #mwi = Utils.GetConfig
        prefix = ''
        '''
        if self.type == 'extension':
            self.subdir = 'extensions'
            loadmethod = 'wfLoadExtension'
            if settings.get('legacy-loader', False):
                #loadmethod = 'require_once'
                loadmethod = 'mwiRequireOnce'
                self.load.args = [f'$IP/extensions/{self.ID}/{self.ID}.php']
        if self.type == 'skin':
            repodir = 'skins'
            loadmethod = 'wfLoadSkin'
            if settings.get('legacy-loader', False):
                #loadmethod = 'require_once'
                loadmethod = 'mwiRequireOnce'
                self.load.args = [f'$IP/skins/{self.ID}/{self.ID}.php']
        '''

        repodir = os.path.join(self.subdir, self.git_name)

        if self.variables is None:
            self.variables={}

        config_prefix='wg'
        extjs = os.path.join(repodir, 'extension.json')
        if os.path.isfile(extjs):
            with open(extjs, 'r') as f:
                extjsdata = json.load(f)
                config_prefix = extjsdata.get('config_prefix', 'wg')
                for k,v in extjsdata.get('config').items():
                    if k not in self.variables:
                        self.variables[config_prefix+k] = v

        mwicfg = os.path.join(repodir, 'mwi.yml')
        if os.path.isfile(mwicfg):
            with open(mwicfg, 'r') as f:
                extjsdata = yaml.load(f)
                config_prefix = extjsdata.get('config_prefix', config_prefix)
                for k,v in extjsdata.get('variables').items():
                    vp=v.get('prefix', config_prefix)
                    self.variables[vp+k] = v


        code = self.code
        suffix = ''
        if len(code) > 0 or len(self.variables.keys()) > 0:
            os_utils.ensureDirExists(localconfig.conf_d_dir, noisy=True)
            actual_config_file = os.path.join(localconfig.conf_d_dir, self.ID+'.settings.php')
            loaded_config_file = '/'.join(['$IP', 'conf.d', self.ID+'.settings.php'])
            if os.path.isfile(actual_config_file):
                actual_config_file = os.path.join(localconfig.conf_d_dir, self.ID+'.settings.new.php')
            with open(actual_config_file, 'w') as cf:
                cf.write(u'<?php\n')
                cf.write(u'// Protect against web entry\n')
                cf.write(u'if ( !defined(\'MEDIAWIKI\') ) { exit; }\n')
                cf.write(u'\n\n')
                #cf.write('{}({});\n\n'.format(self.load.method, ', '.join(self.load.args)))
                k_written=[]
                for line in code:
                    line = line.replace('{{REPO_DIR}}', repodir)
                    cf.write('{}\n'.format(line))
                for k,var in self.variables.items():
                    desc = var.get('description', [])
                    if isinstance(desc, str):
                        desc = [desc]
                    cf.write('\n')
                    for descline in desc:
                        descline=descline.strip()
                        cf.write(f'# {descline}\n')
                    value = php_var_export(var['value'])
                    cf.write(f'${k} = {value}\n')
            loaded_config_file = php_var_export(loaded_config_file, False)
            suffix = f" require_once({loaded_config_file});"
        loadargs=', '.join([php_var_export(x, False) for x in self.load.args])
        w.writeline(f"/* @mwi: extension {self.ID} */ {prefix}{self.load.method}({loadargs});{suffix}")

    def install(self, localconfig):
        return

class MWExtensionLibrary(object):
    ALL = {}
    LOADERID = {}
    LOADED = False

    @classmethod
    def Load(cls, path: str = None):
        if path is None:
            path = os.path.join(MWI_DIR, 'extensions.yml')
        if cls.LOADED:
            return
        with open(path, 'r') as f:
            for k,v in yaml.load(f).items():
                mwext = MWExtension()
                mwext.deserialize(k, v)
                cls.ALL[k]=mwext
                cls.LOADERID[mwext.load.args[0]]=k
                for oldID in mwext.load.deprecated:
                    cls.LOADERID[oldID]=k

    @classmethod
    def GetByID(cls, extID):
        cls.Load()
        return cls.ALL[extID]

    @classmethod
    def GetByLoaderArg(cls, arg):
        cls.Load()
        try:
            return cls.ALL[cls.LOADERID[arg]]
        except KeyError as e:
            #log.critical(e)
            log.info('LOADERID: %r', cls.LOADERID)
            #log.info('ALL: %r', cls.ALL)
            raise e

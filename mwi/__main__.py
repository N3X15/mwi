import logging
import json
import argparse
import codecs
import collections
import os
import random
import re
import shutil
import string
import sys
import subprocess
import pygit2

from pathlib import Path

from typing import List, Tuple, Any, Optional

from semantic_version import Version

from ruamel.yaml import YAML

from mwi.constants import MW_CORE_URI
yaml = YAML(typ='rt')


from buildtools import log, os_utils
from buildtools.config import BaseConfig, YAMLConfig
from buildtools.indentation import IndentWriter
from buildtools.wrapper.git import Git
from buildtools.http import DownloadFile

from argparse import Namespace

SCRIPT_DIR = os.path.abspath('.')

#sys.path.insert(0, os.path.join(SCRIPT_DIR, 'src', 'python'))

from mwi.old_config import MWConfig
from mwi.extension import MWExtensionLibrary

CONFIG = YAMLConfig(os.path.join(SCRIPT_DIR, 'mwUpdate.yml'), {
    'mysql': {
        'host': 'localhost',
        'maint': {
            'admin': 'root',
            'passwd': '',
        }
    },
    'filesystem': {
        'pathtemplate': '/host/{HOST}/htdocs/w',
        'chmod': {
            '*': '755',
            '/LocalSettings.php': '600',
            '/images': '700',
        }
    },
    'defaults': {
        'localconfig':{}
    }
})

CATALOG = YAMLConfig(os.path.join(SCRIPT_DIR, '.catalog.yml'), {'installed': []})

# $wgVersion = '1.34.0';
REG_DEF_VERSION_VAR=re.compile(r"\$wgVersion = '([^']+)';")
# define( 'MW_VERSION', '1.35.0-alpha' );
REG_DEF_VERSION_DEFINE=re.compile(r"define\( 'MW_VERSION', '([^']+)' \);") # 1.34.1+
def getVersion():
    current_version: Optional[str] = None
    with log.info('Getting version data...'):
        with open(os.path.join('includes','DefaultSettings.php'),'r') as f:
            for line in f:
                m = REG_DEF_VERSION_VAR.search(line)
                if m is not None:
                    current_version=Version(m.group(1))
                    break
        if current_version is None:
            with open(os.path.join('includes','Defines.php'),'r') as f:
                for line in f:
                    m = REG_DEF_VERSION_DEFINE.search(line)
                    if m is not None:
                        current_version=Version(m.group(1))
                        break
        assert current_version is not None
        log.info('MediaWiki %s',current_version)
    return current_version

class AddonType(object):
    GLOBAL = 'global'
    EXTENSION = 'extensions'
    LEGACY_EXTENSION = 'legacy-extensions'
    SKIN = 'skins'


class DBConnect(object):

    def __init__(self, user, passwd, schema):
        self.user = user
        self.passwd = passwd
        self.schema = schema
        self.db = None
        self.cursor = None

    def __enter__(self):
        if pymysql is not None:
            pymysql.DEBUG = True
            self.db = pymysql.connect(
                host=CONFIG.get('mysql.host', '127.0.0.1'),
                user=self.user,
                password=self.passwd,
                database=self.schema)
        else:
            mysqldriver = MYSQLdb or pymysql
            nargs = [
                CONFIG.get('mysql.host', '127.0.0.1'),
                self.user,
                self.passwd,
                self.schema
            ]
            mysqldriver.DEBUG = True
            #print(nargs)
            self.db = mysqldriver.connect(*nargs)
        self.cursor = self.db.cursor()
        return self

    def __exit__(self, _type, value, tb):
        # self.db.disconnect()
        pass

    def GetCursor(self):
        return self.cursor

    def Execute(self, sql, params=None):
        #return self.db.query(sql % params)
        return self.cursor.execute(sql, params)


def genpass(size=20, chars=string.ascii_uppercase + string.ascii_uppercase.lower() + string.digits + '_!@#$^&*()-='):
    return ''.join(random.choice(chars) for _ in range(size))

SUDO = os_utils.assertWhich('sudo')
GIT = os_utils.assertWhich('git')
PHP = os_utils.assertWhich('php')

def sudoize(cmd: List[str]) -> List[str]:
    return [SUDO, '-u', 'www-data', '-H']+cmd

def sudo_git(cmd: List[str]) -> List[str]:
    # 'sudo', '-u', 'www-data', 'git'
    return sudoize([GIT]+cmd)

def INSTALL(_args):
    wikidir = CONFIG.get('filesystem.pathtemplate').format(HOST=_args.host)
    os_utils.ensureDirExists(wikidir, noisy=True)
    os_utils.cmd(['chmod', '-R', CONFIG.get('filesystem.chmod.*', '700'), wikidir], echo=True, critical=True)
    os_utils.cmd(['chown', '-R', 'www-data', wikidir], echo=True, critical=True)
    with os_utils.Chdir(wikidir):
        if not os.path.isfile(os.path.join(wikidir, "index.php")):
            os_utils.cmd(sudo_git(['clone', MW_CORE_URI, '.']), echo=True, show_output=True, critical=True)
        os_utils.cmd(sudoize([PHP, '/usr/local/bin/composer', 'install', '--no-dev']), echo=True, show_output=True, critical=True)

        extensions={}
        with open(os.path.join(SCRIPT_DIR, 'extensions.yml'), 'r') as f:
            extensions=yaml.full_load(f)

        localcfg = YAMLConfig(os.path.join(wikidir, '.mwi-config.yml'), {
            'mysql': {
                'user': _args.db_user,
                'pass': genpass()
            },
            'initial-admin': None,
            'localconfig': CONFIG.get('defaults.localconfig', {}),
            'extensions': [k for k,v in extensions.items() if v.get('install-by-default',False)]
        })
        DB_HOST = CONFIG.get('mysql.host')
        DB_USER = localcfg.get('mysql.user')
        DB_PASS = localcfg.get('mysql.pass')
        with DBConnect(CONFIG.get('mysql.maint.user'), CONFIG.get('mysql.maint.passwd'), 'test') as db:
            db.Execute("DROP USER IF EXISTS %s@%s;", (DB_USER, DB_HOST))
            db.Execute("CREATE USER %s@%s IDENTIFIED BY %s;", (DB_USER, DB_HOST, DB_PASS))
            db.Execute("GRANT USAGE ON *.* TO %s@%s REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;", (DB_USER, DB_HOST))
            db.Execute(f"CREATE DATABASE IF NOT EXISTS `{DB_USER}`;")
            db.Execute(f"GRANT ALL PRIVILEGES ON `{DB_USER}`.* TO %s@%s;", (DB_USER, DB_HOST))


def LoadCodeForExt(path):
    code = []
    with codecs.open(path, 'r') as f:
        for line in f:
            code += [line.rstrip()]
    return code

def log_multiline(o: str, level: int) -> None:
    for line in o.splitlines():
        log._log(level, line, [])


def errchk(o: str) -> None:
    for line in o.splitlines():
        if line.startswith('PHP Fatal error:'):
            with log.critical('FATAL PHP ERROR'):
                log_multiline(o, logging.CRITICAL)
            sys.exit(1)
    log_multiline(o, logging.INFO)
    
    


def LoadConfigs(_args: Namespace, for_completion: bool = False) -> Tuple[MWConfig, list]:
    MWExtensionLibrary.Load()

    localcfg = MWConfig.Get(_args.host, CONFIG, SCRIPT_DIR, for_completion)
    localcfg.save()

    all_sites_file = os.path.join(SCRIPT_DIR, 'all-sites.yml')
    all_sites = []
    if os.path.isfile(all_sites_file):
        with open(all_sites_file, 'r') as f:
            asdata = list(yaml.load_all(f))
            if len(asdata) > 0:
                asversion, all_sites = asdata
                if asversion != 1:
                    all_sites = []
    if _args.host not in all_sites:
        all_sites += [_args.host]
    with open(all_sites_file, 'w') as f:
        yaml.dump_all([1,all_sites], f)
    return localcfg, [PHP]

def strToChmod(_in: Any) -> str:
    if type(_in) == int:
        return f'{_in:03d}'
    elif type(_in) == str:
        if _in.startswith('0o'):
            return _in[2:]
        elif _in.startswith('0x'):
            return f'{int(_in):03d}'
        else:
            return _in
    else:
        log.critical('ERROR: strToChmod cannot grok %r (%s)', repr(_in), type(_in))
        return '755'

def fixPerms(wikidir, localcfg):
    globalowner = CONFIG.get('filesystem.dirs.*.owner', 'www-data')
    globalgroup = CONFIG.get('filesystem.dirs.*.group', 'www-data')
    globalmode = CONFIG.get('filesystem.dirs.*.mode',   '755') #(octal)
    os_utils.cmd(['chown', '-R', globalowner+':'+globalgroup, wikidir], echo=True, critical=True)
    os_utils.cmd(['chmod', '-R', strToChmod(globalmode), wikidir], echo=True, critical=True)
    for k, v in CONFIG.get('filesystem.dirs').items():
        if k == '*':
            continue
        if k.startswith('/'):
            k = k[1:]
        cpath = os.path.join(wikidir, k)
        mode = strToChmod(v.get('mode', globalmode))
        if v.get('required', True) and not os.path.isfile(cpath) and not os.path.isdir(cpath):
            os_utils.ensureDirExists(cpath, noisy=True)

        owner = v.get('owner', globalowner)
        group = v.get('group', globalgroup)
        os_utils.cmd(['chown', '-R', f'{owner}:{group}', cpath], echo=True, critical=True)
        os_utils.cmd(['chmod', '-R', mode, cpath], echo=True, critical=True)

def GET_CONFIG(_args: argparse.Namespace):
    localcfg, executor = LoadConfigs(_args, for_completion=True)
    wikidir = localcfg.path

    localcfg.config.get('wg')


def COMPLETE(_args):
    localcfg, executor = LoadConfigs(_args, for_completion=True)
    wikidir = localcfg.path

    DB_HOST = CONFIG.get('mysql.host')
    DB_USER = str(localcfg.mysql_username)
    DB_PASS = str(localcfg.mysql_password)

    wikiuri = f'https://{localcfg.ID}'
    wikiname = _args.wikiname
    username = ''
    password = ''
    if localcfg.config.get('initial-admin', None) is None:
        username = _args.username
        password = genpass(20)
    else:
        username = localcfg.admin_username
        password = localcfg.admin_password

    with os_utils.Chdir(wikidir):
        if _args.redo:
            log.info('Deleting LocalSettings.php...')
            if os.path.isfile(os.path.join(wikidir, 'LocalSettings.new.php')):
                os.remove(os.path.join(wikidir, 'LocalSettings.new.php'))
            if os.path.isfile(os.path.join(wikidir, 'LocalSettings.php')):
                os.remove(os.path.join(wikidir, 'LocalSettings.php'))
            log.info('Emptying database...')
            DB_HOST = CONFIG.get('mysql.host')
            DB_USER = str(localcfg.mysql_username)
            DB_PASS = str(localcfg.mysql_password)
            with DBConnect(CONFIG.get('mysql.maint.user'), CONFIG.get('mysql.maint.passwd'), 'test') as db:
                db.Execute("DROP DATABASE IF EXISTS `%s`;" % (DB_USER,))
                db.Execute("CREATE DATABASE IF NOT EXISTS `%s`;" % (DB_USER,))
            log.info('Done.')

        if not os.path.isfile('LocalSettings.php'):
            cmd = ['maintenance/install.php',
                   '--server=' + wikiuri,
                   '--dbuser=' + DB_USER,
                   '--dbpass=' + DB_PASS,
                   '--dbname=' + DB_USER,
                   '--installdbuser=' + DB_USER,
                   '--installdbpass=' + DB_PASS,
                   '--scriptpath=/w',
                   '--pass=' + password,
                   wikiname,
                   username]
            # print(repr(cmd))
            os_utils.cmd(['sudo', '-u', 'www-data'] + executor + cmd, echo=True, show_output=True, critical=True, globbify=False)

        localcfg.importLocalSettings()

        localcfg.extensions=[]
        localcfg.skins=[]
        # Install extensions
        for extID, ext in MWExtensionLibrary.ALL.items():

            if not ext.install_by_default:
                continue

            if ext.type == 'extension':
                localcfg.extensions += [ext]
            if ext.type == 'skin':
                localcfg.skins += [ext]

            extsubdir=ext.type+'s'

            extdir = os.path.join(wikidir, extsubdir)

            with os_utils.Chdir(extdir):
                subdir = ext.subdir
                destdir = repodir = os.path.join(extdir, extID)
                if subdir != '/':
                    if subdir.startswith('/'):
                        subdir=subdir[1:]
                    repodir=os.path.join(wikidir, '.mwi', 'ext-repo', extID)
                # Check repodir sanity
                if os.path.isdir(repodir) and (_args.redo or not os.path.isdir(os.path.join(repodir,'.git'))):
                    shutil.rmtree(repodir)
                # Check if cloning is required.
                repo_uri = ext.git_repo
                repo_uri=repo_uri.replace('{SUBDIR}', extsubdir)
                repo_uri=repo_uri.replace('{EXTNAME}', ext.git_name)
                if not os.path.isdir(repodir):
                    os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'clone', repo_uri, repodir], echo=True, show_output=True, critical=False)
                    if subdir != '/':
                        if os.path.islink(destdir):
                            os.remove(destdir)
                        elif os.path.isfile(destdir):
                            os.remove(destdir)
                        elif os.path.isdir(destdir):
                            os_utils.safe_rmtree(destdir)
                            shutil.rmtree(destdir, True)
                        os_utils.cmd(['sudo', '-u', 'www-data', 'ln', '-svf', repodir, destdir], echo=True, show_output=True, critical=False)
                # Upgrade
                if os.path.isdir(repodir) and (_args.redo or not os.path.isdir(os.path.join(repodir,'.git'))):
                    shutil.rmtree(repodir)
                if not os.path.isdir(repodir):
                    os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'clone', repo_uri, repodir], echo=True, show_output=True, critical=False)
                    if subdir != '/':
                        if os.path.islink(destdir):
                            os.remove(destdir)
                        elif os.path.isfile(destdir):
                            os.remove(destdir)
                        elif os.path.isdir(destdir):
                            os_utils.safe_rmtree(destdir)
                            shutil.rmtree(destdir, True)
                        os_utils.cmd(['sudo', '-u', 'www-data', 'ln', '-svf', repodir, destdir], echo=True, show_output=True, critical=False)

        fixPerms(wikidir, localcfg)

        with log.info('Writing LocalSettings.new.php...'):
            localcfg.save()
            localcfg.saveLocalSettings()

        with log.info('Installed.'):
            log.info('Username: %s', username)
            log.info('Password: %s', password)
        log.info('Remember to move LocalSettings.new.php to LocalSettings.php!')

def UPGRADE_ALL(_args):
    log.info('Test')
    all_sites_file = os.path.join(SCRIPT_DIR, 'all-sites.yml')
    all_sites = []
    if os.path.isfile(all_sites_file):
        with open(all_sites_file, 'r') as f:
            asdata = list(yaml.load_all(f))
            if len(asdata) == 2:
                asversion, all_sites = asdata
                if asversion != 1:
                    all_sites = []
    for host in all_sites:
        with log.info('Upgrading %s...', host):
            try:
                o = Namespace(host=host, use_hhvm=getattr(_args, 'use_hhvm', False))
                print(repr(o))
                UPGRADE(o)
            except Exception as e:
                log.exception(e)

REG_VERSION = re.compile(r'^\s*origin/REL(\d+)_(\d+)$')
def UPGRADE(_args):
    global REG_VERSION
    localcfg, executor = LoadConfigs(_args)
    wikidir = localcfg.path

    def pygit2Shit() -> None:  # Making sure pygit cleans up after itself.
        # git repo maintenance and idiot checks
        repo = pygit2.Repository(os.path.join(wikidir, '.git'))
        if repo.remotes['origin'].url != MW_CORE_URI:
            log.info('Remote URL: %s -> %s',
                    repo.remotes['origin'].url, MW_CORE_URI)
            repo.remotes.set_url('origin', MW_CORE_URI)
    pygit2Shit()


    with os_utils.Chdir(wikidir):
        versions = []
        os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'fetch', '--all', '--prune', '--tags'], echo=True, show_output=True, critical=True, globbify=False)
        stdout, stderr = os_utils.cmd_output(['sudo', '-u', 'www-data', 'git', 'branch', '-r'], echo=True, critical=True, globbify=False)
        for line in (stderr + stdout).decode('utf-8').splitlines():
            #print(repr(line))
            m = REG_VERSION.match(line)
            if m is not None:
                v = Version('{}.{}.0'.format(int(m.group(1)), int(m.group(2))))
                log.info('%s -> %s', line.strip(), v)
                versions += [v]
        versions.sort(reverse=True)
        v = versions[0]
        branchname = 'REL{}_{}'.format(v.major, v.minor)
        with log.info('Identified %d versions', len(versions)):
            for v in versions:
                log.debug(v)
        log.info('Latest release: %s', branchname)
        #log.info('Would execute %s',' '.join(cmd))
        # vendor is now a submodule. Causes issues on upgrades.
        vendordir = Path('vendor')
        if vendordir.is_dir() and not (vendordir / '.git').is_file():
            log.warning('vendor/ exists but is not a submodule, making way for submodule!')
            shutil.rmtree(vendordir)


        MW_DIR = Path.cwd()
        COMPOSER_JSON = MW_DIR / 'composer.json'
        COMPOSER_LOCK = MW_DIR / 'composer.lock'
        COMPOSER_LOCAL_JSON = MW_DIR / 'composer.local.json'
        COMPOSER_LOCAL_JSON_DATA = {
            "extra": {
                "merge-plugin": {
                    "include": [
                        "extensions/*/composer.json",
                        "skins/*/composer.json"
                    ]
                }
            }
        }

        os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'reset', '--hard'], echo=True, show_output=True, critical=True, globbify=False)
        #os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'submodule', 'foreach', '--recursive', 'git', 'reset', '--hard'], echo=True, show_output=True, critical=True, globbify=False)
        os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'submodule', 'foreach', '--recursive', 'git', 'clean', '-fdx'], echo=True, show_output=True, critical=True, globbify=False)
        os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'submodule', 'foreach', '--recursive', 'git', 'reset', '--hard'], echo=True, show_output=True, critical=True, globbify=False)
        
        if Git.GetBranch() != branchname:
            cmd = 'git checkout -B {0} origin/{0}'.format(branchname).split(' ')
            os_utils.cmd(['sudo', '-u', 'www-data'] + cmd, echo=True, show_output=True, critical=True, globbify=False)
        else:
            os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'pull'], echo=True, show_output=True, critical=True, globbify=False)
        os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'submodule', 'update', '--init', '--recursive'], echo=True, show_output=True, critical=True, globbify=False)

        with COMPOSER_LOCAL_JSON.open('w') as f:
            json.dump(COMPOSER_LOCAL_JSON_DATA, f)
        if COMPOSER_LOCK.is_file():
            COMPOSER_LOCK.unlink()

        fixPerms(wikidir, localcfg)
        current_version=getVersion()
        exts = localcfg.extensions+localcfg.skins
        if exts is None or len(exts) == 0:
            '''
            with log.info('Updating extensions...'):
                for extname in os.listdir('extensions'):
                    dirname = os.path.join(wikidir, 'extensions', extname)
                    #print(wikidir, extname, dirname, branchname, current_version, localcfg['extensions'].get(extname, {}))
                    if os.path.isdir(dirname):
                        upgradeExt(wikidir, extname, "", branchname, current_version, localcfg['extensions'].get(extname, {}))

            with log.info('Updating skins...'):
                for extname in os.listdir('skins'):
                    dirname = os.path.join(wikidir, 'skins', extname)
                    if os.path.isdir(dirname):
                        upgradeExt(wikidir, extname, "", branchname, current_version, localcfg['extensions'].get(extname, {}))
            '''
            localcfg.importLocalSettings()
            localcfg.save()
            exts = localcfg.extensions+localcfg.skins

        for ext in exts:
            upgradeExt(wikidir, ext.ID, "", branchname, current_version, ext)

        localcfg.save()
        localcfg.saveLocalSettings()

        # os_utils.cmd([COMPOSER, 'u', '--no-dev', '--ignore-platform-reqs', '--no-interaction', '--no-plugins'], echo=True, show_output=True, critical=True, env=env)
        os_utils.cmd(sudoize(executor + ['/usr/local/bin/composer', 'update', '-o', '--no-dev', '--no-interaction', '--no-plugins']), echo=True, show_output=True, critical=True, globbify=False)
        os_utils.cmd(sudoize(executor + ['/usr/local/bin/composer', 'update', '-o', '--no-dev', '--no-interaction']), echo=True, show_output=True, critical=True, globbify=False)
        errchk(os_utils.cmd_out(['sudo', '-u', 'www-data'] + executor + ['maintenance/update.php', '--quick'], echo=True, critical=True, globbify=False))

def upgradeExt(wikidir, extname,dirname,branchname,current_version,ext):
    extsubdir = 'extensions'
    if ext.type == 'skin':
        extsubdir = 'skins'
    extdir = os.path.join(wikidir, extsubdir)
    destdir = repodir = os.path.join(extdir, extname)
    if ext.subdir != '/':
        destdir = os.path.join(wikidir, ext.subdir)
    #repodir = os.path.join(wikidir, '.mwi', 'ext-repo', extname)
    # Check if cloning is required.
    repo_uri=ext.git_repo
    if repo_uri is not None:
        repo_uri=repo_uri.replace('{SUBDIR}', extsubdir)
        repo_uri=repo_uri.replace('{EXTNAME}', ext.git_name)
        while True:
            configured_repo_uri = None
            if os.path.isfile(os.path.join(repodir, '.git')):
                log.warn('Submodule, not fucking with it.')
                return
            if os.path.isdir(os.path.join(repodir, '.git')):
                with os_utils.Chdir(repodir, quiet=True):
                    #Fetch URL: https://github.com/git/git
                    stdout, stderr = os_utils.cmd_output(['sudo', '-u', 'www-data', 'git', 'remote', 'show', 'origin'], echo=True, critical=True)
                    for oline in (stdout+stderr).decode('utf-8').split('\n'):
                        line = oline.strip()
                        components = line.split()
                        if line.startswith('Fetch URL:'):
                            configured_repo_uri = components[2]
            # Check repodir sanity
            if os.path.isdir(repodir):
                bad = False
                if configured_repo_uri != repo_uri:
                    log.warn('Configured Remote URI: %s', repo_uri)
                    log.warn('Present Remote URI...: %s', configured_repo_uri)
                    bad=True
                if not bad and not os.path.isdir(os.path.join(repodir,'.git')):
                    log.warn('.git/ is missing!')
                    bad=True
                if not bad:
                    # Let's dive right into this bitch.
                    repo = pygit2.Repository(os.path.join(repodir, '.git'))
                    # Test 1: origin/master is not a local branch.
                    for branchName in repo.branches.local:
                        if branchName.startswith('origin/'):
                            log.warn(f'{branchName} exists as a local branch!')
                            bad = True
                    
                if bad:
                    log.error('Resetting repo.')
                    if os.path.islink(repodir):
                        os.unlink(repodir)
                    elif os.path.isfile(repodir):
                        os.remove(repodir)
                    elif os.path.isdir(repodir):
                        shutil.rmtree(repodir)
                    log.error('Trying to upgrade again...')
                    continue
            if not os.path.isdir(os.path.join(repodir, '.git')):
                os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'clone', '--recursive', repo_uri, repodir], echo=True, show_output=True, critical=False)
                if ext.subdir != '/':
                    if os.path.islink(destdir):
                        os.remove(destdir)
                    elif os.path.isfile(destdir):
                        os.remove(destdir)
                    elif os.path.isdir(destdir):
                        os_utils.safe_rmtree(destdir)
                        shutil.rmtree(destdir, True)
                    os_utils.cmd(['sudo', '-u', 'www-data', 'ln', '-svf', os.path.join(repodir, ext.subdir), destdir], echo=True, show_output=True, critical=False)
                continue
            else:
                with os_utils.Chdir(repodir):
                    os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'clean', '-fdx'], echo=True, show_output=True, critical=True, globbify=False)
                    os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'reset', '--hard'], echo=True, show_output=True, critical=True, globbify=False)
                    # Fix HEAD ref issues.
                    os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'checkout', '-B', 'master', 'origin/master'], echo=True, show_output=True, critical=True, globbify=False)
                    os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'fetch', '--all', '--prune', '--tags'], echo=True, show_output=True, critical=True, globbify=False)
                    stdout, stderr = os_utils.cmd_output(['sudo', '-u', 'www-data', 'git', 'branch', '-r'], echo=False, critical=True, globbify=False)
                    found=False
                    for line in (stderr + stdout).decode('utf-8').splitlines():
                        line=line.strip()
                        #print(repr(line))
                        if 'origin/'+branchname == line:
                            if Git.GetBranch() != branchname:
                                cmd = 'git checkout -B {0} origin/{0}'.format(branchname).split(' ')
                                os_utils.cmd(['sudo', '-u', 'www-data'] + cmd, echo=True, show_output=True, critical=True, globbify=False)
                            else:
                                os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'pull'], echo=True, show_output=True, critical=True, globbify=False)
                            found=True
                            break
                    if not found:
                        stdout, stderr = os_utils.cmd_output(['sudo', '-u', 'www-data', 'git', 'tag', '-l'], echo=False, critical=True, globbify=False)
                        for line in (stderr + stdout).decode('utf-8').splitlines():
                            line=line.strip()
                            print(repr(line))
                            if 'origin/'+str(current_version) == line:
                                cmd = 'git checkout -B {0} origin/{0}'.format(current_version).split(' ')
                                os_utils.cmd(['sudo', '-u', 'www-data'] + cmd, echo=True, show_output=True, critical=True, globbify=False)
                                found=True
                                break
                    if not found:
                        cmd = 'git checkout -B {0} origin/{0}'.format('master').split(' ')
                        os_utils.cmd(['sudo', '-u', 'www-data'] + cmd, echo=True, show_output=True, critical=True, globbify=False)

            if os.path.isfile(os.path.join(repodir, '.gitmodules')):
                with os_utils.Chdir(repodir):
                    os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'submodule', 'update', '--init', '--recursive'], echo=True, show_output=True, critical=True, globbify=False)

            for k, v in ext.chmod.items():
                if k == '*':
                    continue
                if k.startswith('/'):
                    k = k[1:]
                cpath = os.path.join(repodir, k)
                os_utils.cmd(['chmod', '-Rv', v, cpath], echo=True, show_output=True, critical=True)
            break

def INSTALL_EXT(_args):
    current_version=None
    localcfg, _ = LoadConfigs(_args)
    wikidir = localcfg.path
    with os_utils.Chdir(localcfg.path):
        newExts = _args.new_ext
        if len(newExts) == 0:
            return
        abort = False
        for extName in newExts:
            if extName not in MWExtensionLibrary.ALL:
                log.critical('Extension %r is not defined in extensions library.', extName)
                abort = True
                break
        if abort:
            log.error('Aborting.')
            return
        fixPerms(wikidir, localcfg)
        current_version = getVersion()
        for extID in newExts:
            mwext = MWExtensionLibrary.GetByID(extID)
            if mwext.type == 'skin':
                localcfg.skins += [mwext]
            else:
                localcfg.extensions += [mwext]
            if mwext.git_repo is None:
                continue
            repo_uri = mwext.git_repo
            extsubdir = mwext.type+'s'
            extdir = os.path.join(wikidir, extsubdir)
            destdir = repodir = os.path.join(extdir, mwext.ID)
            repo_uri=repo_uri.replace('{SUBDIR}', extsubdir)
            repo_uri=repo_uri.replace('{EXTNAME}', mwext.git_name)
            subdir = mwext.subdir
            if subdir != '/':
                if subdir.startswith('/'):
                    subdir=subdir[1:]
                repodir=os.path.join(wikidir, '.mwi', 'ext-repo', mwext.ID)
            with os_utils.Chdir(extdir):
                gitdir = os.path.join(repodir, '.git')
                configured_repo_uri = None
                if os.path.isdir(os.path.join(repodir, '.git')):
                    with os_utils.Chdir(repodir):
                        #Fetch URL: https://github.com/git/git
                        stdout, stderr = os_utils.cmd_output(['sudo', '-u', 'www-data', 'git', 'remote', 'show', 'origin'], echo=True, critical=True)
                        for oline in (stdout+stderr).decode('utf-8').split('\n'):
                            line = oline.strip()
                            components = line.split()
                            if line.startswith('Fetch URL:'):
                                configured_repo_uri = components[2]
                # Check repodir sanity
                if os.path.isdir(repodir):
                    bad = False
                    if configured_repo_uri != repo_uri:
                        log.warn('Configured Remote URI: %s', repo_uri)
                        log.warn('Present Remote URI...: %s', configured_repo_uri)
                        bad=True
                    if not bad and not os.path.isdir(os.path.join(repodir,'.git')):
                        log.warn('.git/ is missing!')
                        bad=True
                    if bad:
                        shutil.rmtree(repodir)
                if not os.path.isdir(repodir):
                    os_utils.cmd(['sudo', '-u', 'www-data', 'git', 'clone', repo_uri.format(mwext.ID), repodir], echo=True, show_output=True, critical=False)
                    if subdir != '/':
                        if os.path.islink(destdir):
                            os.remove(destdir)
                        elif os.path.isfile(destdir):
                            os.remove(destdir)
                        elif os.path.isdir(destdir):
                            os_utils.safe_rmtree(destdir)
                            shutil.rmtree(destdir, True)
                        os_utils.cmd(['sudo', '-u', 'www-data', 'ln', '-svf', os.path.join(repodir, subdir), destdir], echo=True, show_output=True, critical=False)

        CONF_D_DIR = os.path.join(wikidir,'conf.d')
        with log.info('Writing LocalSettings.php...'):
            localcfg.save()
            localcfg.saveLocalSettings()
        #os.remove(os.path.join(wikidir, 'LocalSettings.php'))
        #shutil.move(os.path.join(wikidir, 'LocalSettings.new.php'), os.path.join(wikidir, 'LocalSettings.php'))

    branchname=f'REL{current_version.major}_{current_version.minor}'
    for key in newExts:
        if key in MWExtensionLibrary.ALL:
            mwext = MWExtensionLibrary.GetByID(key)
            repo_uri = mwext.git_repo
            if repo_uri is None:
                continue
            extsubdir = mwext.type+'s'
            extdir = os.path.join(wikidir, extsubdir)
            subdir = mwext.subdir
            destdir = repodir = os.path.join(extdir, mwext.ID)
            repo_uri=repo_uri.replace('{SUBDIR}', extsubdir)
            repo_uri=repo_uri.replace('{EXTNAME}', mwext.git_name)
            if subdir != '/':
                if subdir.startswith('/'):
                    subdir=subdir[1:]
                repodir=os.path.join(wikidir, '.mwi', 'ext-repo', mwext.ID)
            upgradeExt(wikidir, key, extdir, branchname, current_version, mwext)

def IMPORT(_args):
    package = _args.package
    if package is not None:
        package = os.path.abspath(package)
    #wikidir, localcfg, executor = LoadConfigs(_args)
    localcfg, _ = LoadConfigs(_args)
    with os_utils.Chdir(wikidir):
        articles = []
        for articlename in _args.article:
            chunks = articlename.split(_args.split_char)
            if len(chunks) == 1:
                chunks=[chunks[0],chunks[0]]
            articles += [chunks[:2]]
        if package is not None:
            sc = _args.split_char
            with open(package, 'r') as f:
                for line in f:
                    line=line.strip()
                    if line.startswith('@splitchar='):
                        sc = line[11]
                        continue
                    chunks = line.strip().split(sc)
                    if len(chunks) == 1:
                        chunks=[chunks[0],chunks[0]]
                    articles += [chunks[:2]]
        for source, destination in articles:
            DownloadFile(f'{_args.source}?title={source}&action=raw', '/tmp/mwi_article.txt')
            cmd = ['sudo', '-u', 'www-data'] + executor + ['maintenance/edit.php', '-b', destination, '-s', 'Importing from another wiki']
            with open('/tmp/mwi_article.txt', 'rb') as f:
                process = subprocess.Popen(cmd, stdin=subprocess.PIPE)
                process.stdin.write(f.read())
                print(process.communicate())

def cmd_list(_args):
    all_sites_file = os.path.join(SCRIPT_DIR, 'all-sites.yml')
    all_sites = []
    with open(all_sites_file, 'r') as f:
        data = list(yaml.load_all(f))
        assert len(data) == 2
        version, sitelist = data
        assert version == 1
        print(json.dumps(sorted(sitelist), indent=2))

def main():
    argp = argparse.ArgumentParser()
    argp.add_argument('--use-hhvm', action='store_true', default=False, help='Use the HHVM interpreter instead of PHP.')
    subp = argp.add_subparsers()

    installp = subp.add_parser('install', help='Begin installation of a wiki')
    installp.add_argument('host', help='hostname, like www.wikipedia.com')
    installp.add_argument('db_user', help='hostname, like www.wikipedia.com')
    installp.set_defaults(func=INSTALL)

    completep = subp.add_parser('complete', help='Complete installation after initial configuration.')
    completep.add_argument('host', help='hostname, like www.wikipedia.com')
    completep.add_argument('--wikiname', help='wiki name, ex: Wikipedia')
    completep.add_argument('--username', help='Admin user name (password auto-generated).')
    completep.add_argument('--redo', action='store_true')
    completep.set_defaults(func=COMPLETE)

    upgradep = subp.add_parser('upgrade', help='Update an installation to the latest release.')
    upgradep.add_argument('host', help='hostname, like www.wikipedia.com')
    upgradep.set_defaults(func=UPGRADE)
    completep.set_defaults(func=COMPLETE)

    upgradep = subp.add_parser('import', help='Import articles from Wikipedia or some other place.')
    upgradep.add_argument('host', help='destination hostname, like reversing.nexisonline.net')
    upgradep.add_argument('-S', '--split-char', help='Split character', nargs='?', default='=')
    upgradep.add_argument('-s', '--source', help='source hostname, like en.wikipedia.org', nargs='?', default=None)
    upgradep.add_argument('-a', '--article', help='Articles to bring over (old=new)', nargs='*', default=[])
    upgradep.add_argument('-p', '--package', help='Packagefile with articles to import', nargs='?', default=None)
    upgradep.set_defaults(func=IMPORT)

    upgradeallp = subp.add_parser('upgrade-all', help='Update all installations to the latest release.')
    upgradeallp.set_defaults(func=UPGRADE_ALL)

    listp = subp.add_parser('list', help='List all sites.')
    listp.set_defaults(func=cmd_list)

    installext = subp.add_parser('add-ext', help='Install an extension.')
    installext.add_argument('host', help='hostname, like www.wikipedia.com')
    installext.add_argument('new_ext', nargs='+', help='Name of extension package, such as Cite.')
    installext.add_argument('--reinstall', action='store_true')
    installext.set_defaults(func=INSTALL_EXT)

    args = argp.parse_args()
    if hasattr(args, 'func'):
        args.func(args)
    else:
        argp.print_usage()

if __name__ == '__main__':
    main()
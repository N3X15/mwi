'''
A farm is the collection of wiki installation instances.

A farm consists of shared variables (database server, redis, etc) and the collection of wikis itself.
'''

import os
from pathlib import Path
import shutil
from typing import Any, Dict, List, Optional, Set
from buildtools import YAMLConfig

import click
from mwi.mwconfig import MWConfig
from mwi.installation import MWInstallation
from mwi.old_config import OldMWConfig

from mwi.system_user import PosixUser, SystemUser, getSystemUserFromDict

from ruamel.yaml import YAML as Yaml
YAML = Yaml(typ='rt', pure=True)

class WikiFarm:
    def __init__(self) -> None:
        self.webuser: Optional[SystemUser] = None
        self.rootuser: Optional[SystemUser] = None
        
        self.mariadb_user: str = 'root'
        self.mariadb_pass: str = ''
        self.mariadb_host: str = 'localhost'
        self.mariadb_port: int = 3306

        self.path_sudo: Optional[str] = None
        self.path_git: Optional[str] = None
        self.path_composer: Optional[str] = None
        self.path_php: Optional[str] = None

        self.path_template: str = ''

        self.installations: Dict[str, MWInstallation] = {}

        self.required_extensions: Set[str] = set()
        self.replace_extensions: Dict[str,str] = {}

        self.rename_vars: Dict[str,str] = {}
        self.global_vars: Dict[str,Any] = {}

    def locateCommands(self) -> None:
        self.path_sudo = shutil.which('sudo')
        self.path_git      = os.environ.get('GIT_PATH')      or shutil.which('git')
        self.path_composer = os.environ.get('COMPOSER_PATH') or shutil.which('composer')
        self.path_php      = os.environ.get('PHP_PATH')      or shutil.which('php')

    def load(self) -> None:
        self.loadFrom(Path.home() / '.mwi')

    def loadFrom(self, config_path: Path) -> None:
        self.locateCommands()
        self.loadConfigurationFrom(config_path / 'farm.yml')
        self.loadInstallationsFrom(config_path / 'installations')

    def save(self) -> None:
        self.saveTo(Path.home() / '.mwi')

    def saveTo(self, config_path: Path) -> None:
        self.saveConfigurationTo(config_path / 'farm.yml')
        self.saveInstallationsTo(config_path / 'installations')

    def loadConfigurationFrom(self, config_file_path: Path) -> None:
        with config_file_path.open('r') as f:
            data = YAML.load(f)
        self.webuser = None
        self.rootuser = None
        if 'sysusers' in data:
            if 'web-daemon' in data['sysusers']:
                self.webuser = getSystemUserFromDict(data['sysusers']['web-daemon'])
            if 'root' in data['sysusers']:
                self.rootuser = getSystemUserFromDict(data['sysusers']['root'])
        if 'paths' in data:
            self.path_git = data['paths'].get('git', self.path_git)
            self.path_composer = data['paths'].get('composer', self.path_composer)
            self.path_php = data['paths'].get('php', self.path_php)
            self.path_template = data['paths'].get('template', '/host/{hostname}/htdocs')
        if 'mysql' in data:
            self.mariadb_host = data['mysql']['host']
            self.mariadb_port = data['mysql']['port']
            self.mariadb_user = data['mysql']['user']
            self.mariadb_pass = data['mysql']['pass']
        if 'extensions' in data:
            self.required_extensions = set(data['extensions'].get('required',[]))
            self.replace_extensions = data['extensions'].get('replace',{})
        if 'vars' in data:
            self.rename_vars = data['vars'].get('rename',{})
            self.global_vars = data['vars'].get('global',{})

    def saveConfigurationTo(self, config_file_path: Path) -> None:
        data = {
            'sysusers': {
                'web-daemon': self.webuser.serialize(),
                'root': self.rootuser.serialize(),
            },
            'paths': {
                'composer': self.path_composer,
                'git': self.path_git,
                'php': self.path_php,
                'template': self.path_template,
            },
            'mysql': {
                'host': self.mariadb_host,
                'port': self.mariadb_port,
                'user': self.mariadb_user,
                'pass': self.mariadb_pass,
            },
            'extensions': {
                'required': list(sorted(self.required_extensions)),
                'replace': self.replace_extensions,
            },
            'vars': {
                'rename': self.rename_vars,
                'global': self.global_vars,
            }
        }
        tmpfile = config_file_path.with_suffix('.tmp')
        with tmpfile.open('w') as f:
            YAML.dump(data, f)
        os.replace(tmpfile, config_file_path)

    def loadInstallationsFrom(self, installs_dir: Path) -> None:
        OLD_ALLSITES = Path.cwd() / 'all_sites.yml'
        if OLD_ALLSITES.is_file():
            self.importOldSites()
            return

    def importOldSites(self) -> None:
        OLD_ALLSITES = Path.cwd() / 'all_sites.yml'
        all_sites: List[str] = []
        with open(OLD_ALLSITES, 'r') as f:
            asdata = list(YAML.load_all(f))
            if len(asdata) > 0:
                asversion, _all_sites = asdata
                if asversion == 1:
                    all_sites = list(_all_sites)

        mwu = YAMLConfig('mwUpdate.yml')
        
        self.mariadb_host = mwu.get('mysql.host')
        self.mariadb_user = mwu.get('mysql.maint.user')
        self.mariadb_pass = mwu.get('mysql.maint.passwd')

        self.webuser = PosixUser()
        self.webuser.name = 'www-data'
        self.webuser.group = 'www-data'
        
        self.rootuser = PosixUser()
        self.rootuser.name = 'root'
        self.rootuser.group = 'root'
        self.rootuser.uid = 0
        self.rootuser.gid = 0

        self.global_vars = mwu.get('variables.default') or {}
        self.rename_vars = mwu.get('variables.rename') or {}

        self.saveConfiguration()

        self.installations = {}
        n=len(all_sites)
        for i,site in enumerate(all_sites):
            self.importOldSite(mwu, site,i,n)

    def importOldSite(self, mwu: YAMLConfig, siteid: str, i: Optional[int] = None, n: Optional[int] = None) -> None:
        prog = ''
        if n and i:
            prog = click.style(f'[{i}/{n}] ',fg='green')
        siteid = click.style(siteid, fg='cyan')
        click.secho(f'{prog}Importing site {siteid}...')
        cfg: OldMWConfig = OldMWConfig.Get(siteid, mwu, str(Path.cwd().absolute()), False)
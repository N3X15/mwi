from __future__ import annotations
import os
from pathlib import Path
from typing import Any, Dict, Type

from frozendict import frozendict
try:
    import pwd
except:
    pwd = None
try:
    import grp
except:
    grp = None


class SystemUser:
    TYPEID: str
    def __init__(self) -> None:
        self.name: str = ''

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.name = data.get('name')

    def serialize(self) -> Dict[str, Any]:
        return {
            'type': self.TYPEID,
            'name': self.name,
        }

    def take_ownership_of_file(self, file: Path) -> None:
        pass

    def take_ownership_of_dir(self, dirpath: Path) -> None:
        pass

    def take_ownership_of_dir_contents(self, dirpath: Path) -> None:
        for root, files, dirs in os.walk(dirpath):
            for filepathrel in files:
                self.take_ownership_of_file(Path(os.path.join(root,filepathrel)))
            for dirpathrel in dirs:
                self.take_ownership_of_dir(Path(os.path.join(root,dirpathrel)))
        self.take_ownership_of_dir(dirpath)

class PosixUser(SystemUser):
    TYPEID: str = 'posix'
    def __init__(self) -> None:
        super().__init__()
        self.group: str = ''
        self.uid: int = 0
        self.gid: int = 0

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.name = data.get('user-name')
        self.uid = pwd.getpwnam(self.name)

        self.group = data.get('group-name')
        self.gid = grp.getgrnam(self.group)

    def serialize(self) -> Dict[str, Any]:
        return {
            'type': self.TYPEID,
            'user-name': self.name,
            'user-uid': self.uid,
            'group-name': self.group,
            'group-gid': self.gid,
        }

    
    def take_ownership_of_file(self, file: Path) -> None:
        os.chown(file, self.uid, self.gid)

    def take_ownership_of_dir(self, dirpath: Path) -> None:
        os.chown(dirpath,self.uid,self.gid)


TYPEID2TYPE: frozendict[str, Type[SystemUser]] = frozendict({
    PosixUser.TYPEID: PosixUser,
    None: PosixUser,
})

def getSystemUserFromDict(data: Dict[str, Any]) -> SystemUser:
    tid = data.get('type', 'posix' if os.name == 'nt' else None)
    u:SystemUser = TYPEID2TYPE[tid]()
    u.deserialize(data)
    return u
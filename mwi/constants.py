import os
from pathlib import Path

CONFIG_DIR = Path.home() / '.mwi'

MWI_DIR = Path.cwd()
TEMPLATES_DIR = MWI_DIR / 'src'/ 'templates'
VARS2JSON = MWI_DIR / 'vars2json.php'

MW_CORE_URI: str = 'https://gerrit.wikimedia.org/r/mediawiki/core.git/'
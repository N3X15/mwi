from typing import Any, Dict, List, Optional, TextIO, Union
from ._basenode import PHPNode
class PHPIfElse(PHPNode):
    TYPEID = 'if'

    def __init__(self) -> None:
        super().__init__()
        self.expr: Union[str, PHPNode]=''
        self.then: PHPNode = PHPNode()
        self.else_: Optional[PHPNode] = None
    
    def children(self) -> List[PHPNode]:
        return [self.expr, self.then, self.else_]

    def set_children(self, val: List[PHPNode]) -> None:
        self.expr, self.then, self.else_ = val

    def serialize(self) -> Dict[str, Any]:
        o = super().serialize()
        o['expr'] = self.expr.serialize() if isinstance(self.expr, PHPNode) else self.expr
        o['then'] = self.then.serialize()
        if self.else_ is not None:
            o['else'] = self.else_.serialize()
        return o

    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        if isinstance(data['expr'], str):
            self.expr = data['expr']
        else:
            self.expr = PHPNode.FromData(data['expr'])
        self.then = PHPNode.FromData(data['then'])
        if 'else' in data:
            self.else_ = PHPNode.FromData(data['else'])
    
    def write(self, f: TextIO) -> None:
        f.write('if(')
        self.expr.write(f)
        f.write(')')
        self.then.write(f)
        if self.else_ is not None:
            f.write('else')
            self.else_.write(f)
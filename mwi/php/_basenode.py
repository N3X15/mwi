from typing import Any, Dict, List, TextIO, Type
from __future__ import annotations


class PHPNode:
    _TYPES: Dict[str, Any] = {}

    TYPEID: str

    @classmethod
    def Register(cls: Type[PHPNode], typ: Type[PHPNode]) -> None:
        cls._TYPES[typ.TYPEID] = typ

    @classmethod
    def FromData(cls: Type[PHPNode], data: Dict[str, Any]) -> PHPNode:
        n: PHPNode = cls._TYPES[data['type']]()
        n.deserialize(data)
        return n
    
    def __init__(self) -> None:
        pass

    @property
    def children(self) -> List[PHPNode]:
        return []

    @children.setter
    def set_children(self, val: List[PHPNode]) -> None:
        pass

    def serialize(self) -> Dict[str, Any]:
        return {'type': self.TYPEID}

    def deserialize(self, data: dict) -> None:
        pass

    def write(self, f: TextIO) -> None:
        pass
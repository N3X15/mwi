from typing import Any, Dict, List, TextIO
from ._basenode import PHPNode

class PHPAssign(PHPNode):
    TYPEID = 'assign'
    def __init__(self) -> None:
        super().__init__()
        self.left: str = ''
        self.op: str = '='
        self.right: PHPNode = None

    def children(self) -> List[PHPNode]:
        return [self.right]

    def set_children(self, val: List[PHPNode]) -> None:
        self.right = val[0]

    def serialize(self) -> Dict[str, Any]:
        o = super().serialize()
        o['left'] = self.left
        o['op'] = self.op
        o['right'] = self.right.serialize()
        return o

    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.left = data['left']
        self.op = data['op']
        self.right = PHPNode.FromDict(data['right'])

    def write(self, f: TextIO) -> None:
        f.write(f'{self.left} {self.op} ')
        self.right.write(f)
        f.write(';\n')
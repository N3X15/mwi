# MediaWiki Installer (MWI)

This is a crappy tool I threw together to automate installing and updating mediawiki and its extensions and skins.

Rather than pulling archives, it makes use for git to clone and update repositories automatically, and
runs composer, install.php and update.php when needed.  It also intelligently knows which version of MW
is the most recent release, and checks out the matching version of each extension and skin.

Most importantly, when a release of MW is made, MWI can be invoked with `upgrade_all` to automatically perform
upgrades on **all** installed wikis on a server.

## Installation

1. `git clone https://gitlab.com/N3X15/MWI.git /root/mwi` (Yes, you need root.)
1. Install python >= 3.6
1. `pip install -U git+https://gitlab.com/N3X15/python-build-tools.git`
1. `pip install semantic_version`
1. Copy mwUpdate.yml.example to mwUpdate.yml and edit to taste.
1. `python BUILD.py`

## Usage

1. `cd ~/mwi`
1. `python3.6 mwi.py --help`

NOTE:  You must run `mwi.py complete` after `mwi.py install` to initialize the database and install extensions.

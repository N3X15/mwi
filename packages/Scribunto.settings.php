$wgScribuntoDefaultEngine = 'luastandalone';
$wgScribuntoUseCodeEditor = true;

# where lua is the name of the binary file
# e.g. sourceforge LuaBinaries 5.1.5 - Release 2 name the binary file lua5.1
#$wgScribuntoEngineConf['luastandalone']['luaPath'] = '/path/to/binaries/lua5.1';

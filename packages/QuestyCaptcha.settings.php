die('QuestyCaptcha requires configuration.');
$wgCaptchaClass = 'QuestyCaptcha';
$arr = array (
	"A question?" => "An answer!",
	"What is this wiki's name?" => "$wgSitename",
	'Please write the magic secret, "passion", here:' => 'passion',
	'Type the code word, 567, here:' => '567',
	'Which animal? <img src="http://www.mysite.com/dog.jpg" alt="" title="" />' => 'dog',
);
$wgCaptchaQuestions=[];
foreach ( $arr as $key => $value ) {
	$wgCaptchaQuestions[] = array( 'question' => $key, 'answer' => $value );
}
unset($arr);
unset($key);
unset($value);

$wgMakeUserPageFromBio = false;
$wgAutoWelcomeNewUsers = false;
$wgConfirmAccountRequestFormItems = array(
   'UserName'        => array( 'enabled' => true ),
   'RealName'        => array( 'enabled' => false ),
   'Biography'       => array( 'enabled' => false, 'minWords' => 50 ),
   'AreasOfInterest' => array( 'enabled' => false ),
   'CV'              => array( 'enabled' => false ),
   'Notes'           => array( 'enabled' => true ),
   'Links'           => array( 'enabled' => false ),
   'TermsOfService'  => array( 'enabled' => false ),
);

# Restrict account creation
$wgGroupPermissions['*']['createaccount'] = false;
$wgGroupPermissions['user']['createaccount'] = false;
# Grant account queue rights
$wgGroupPermissions['bureaucrat']['confirmaccount'] = true;
# Receive emails when an account confirms its email address
# $wgGroupPermissions['bureaucrat']['confirmaccount-notify'] = true;
# This right has the request IP show when confirming accounts
$wgGroupPermissions['bureaucrat']['requestips'] = true;

# If credentials are stored, this right lets users look them up
$wgGroupPermissions['bureaucrat']['lookupcredentials'] = true;

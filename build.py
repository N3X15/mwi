import json
import os
from pathlib import Path
from buildtools import os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.nuitka import NuitkaTarget
from buildtools.maestro.fileio import CopyFileTarget


bm = BuildMaestro()
argp = bm.build_argparser()
argp.add_argument('mediawiki_branch', type=str, help='e.g. REL1_37')
args = bm.parse_args(argp)

GIT      = os.environ.get('GIT_PATH')      or os_utils.assertWhich('git')
COMPOSER = os.environ.get('COMPOSER_PATH') or os_utils.assertWhich('composer')
PHP      = os.environ.get('PHP_PATH')      or os_utils.assertWhich('php')

os_utils.ensureDirExists('data')
os_utils.ensureDirExists('src/php')
MW_DIR = Path('lib') / 'mediawiki'
COMPOSER_JSON = MW_DIR / 'composer.json'
COMPOSER_LOCK = MW_DIR / 'composer.lock'
COMPOSER_LOCAL_JSON = MW_DIR / 'composer.local.json'
COMPOSER_LOCAL_JSON_DATA = {
    "extra": {
        "merge-plugin": {
            "include": [
                "extensions/*/composer.json",
                "skins/*/composer.json"
            ]
        }
    }
}

os_utils.cmd([GIT, 'submodule', 'foreach', '--recursive', 'git', 'reset', '--hard'], echo=True, show_output=True, critical=True)
os_utils.cmd([GIT, 'submodule', 'foreach', '--recursive', 'git', 'clean', '-fdx'], echo=True, show_output=True, critical=True)
os_utils.cmd([GIT, 'submodule', 'set-branch', '--branch', args.mediawiki_branch, '--', str(MW_DIR)], echo=True, show_output=True, critical=True)
os_utils.cmd([GIT, 'submodule', 'update', '--init', '--remote', '--', str(MW_DIR)], echo=True, show_output=True, critical=True)
#os_utils.cmd([GIT, 'submodule', 'update', '--init', '--recursive', '--', os.path.join('lib','mediawiki')], echo=True, show_output=True, critical=True)

with COMPOSER_LOCAL_JSON.open('w') as f:
    json.dump(COMPOSER_LOCAL_JSON_DATA, f)
if COMPOSER_LOCK.is_file():
    COMPOSER_LOCK.unlink()

with os_utils.Chdir('lib/mediawiki'):
    os_utils.cmd([GIT, 'submodule', 'update', '--init', '--recursive'], echo=True, show_output=True, critical=True)
    # We're building for static analysis purposes, so this is fine.
    env = os_utils.ENV.clone()
    env.set('COMPOSER_ALLOW_SUPERUSER', '1')
    os_utils.cmd([COMPOSER, 'u', '--no-dev', '--ignore-platform-reqs', '--no-interaction', '--no-plugins'], echo=True, show_output=True, critical=True, env=env)
    os_utils.cmd([COMPOSER, 'u', '--no-dev', '--ignore-platform-reqs', '--no-interaction'], echo=True, show_output=True, critical=True, env=env)
os_utils.cmd([PHP, 'dumpMWDefines.php'], echo=True, show_output=True, critical=True)

MWI_DIR = Path('mwi')
MWI_CLI_PY = MWI_DIR / '__main__.py'
n: NuitkaTarget = bm.add(NuitkaTarget(str(MWI_CLI_PY), '__main__', [str(x) for x in MWI_CLI_PY.rglob('*.py')], single_file=True))
n.windows_product_name = 'mwi CLI Tool'
n.windows_company_name = 'MWI Contributors'
n.windows_file_version = (0,0,1,0)
n.windows_file_description = 'a'
n.included_packages.add('pygit2') # Or we get _cffi_* errors
bm.add(CopyFileTarget('bin/mwi', str(n.executable_mangled), [n.target]))
bm.as_app(argp)
<?php
define('MEDIAWIKI', 1);
define('MWI',       1);

include(dirname(__FILE__).'/src/php/MWVars.php');
include(dirname(__FILE__).'/src/php/MWDefines.php');

$_mwiExtensions=[];
$_mwiSkins=[];
$_mwiIncludes=[];

function wfLoadExtension($ext, $path=null) {
    global $_mwiExtensions;
    $_mwiExtensions[]=$ext;
}
function wfLoadExtensions(array $exts) {
    global $_mwiExtensions;
    foreach($exts as $ext) {
        $_mwiExtensions[]=$ext;
    }
}
function wfLoadSkin($skin, $path=null) {
    global $_mwiSkins;
    $_mwiSkins[]=$skin;
}
function wfLoadSkins(array $skins) {
    global $_mwiSkins;
    foreach($skins as $skin){
    $_mwiSkins[]=$skin;
    }
}
function mwiRequireOnce($path) {
    global $_mwiIncludes, $IP;
    $_mwiIncludes[]=str_replace($IP, '$IP', $path);
}
$EXCLUDED_KEYS=[
    'GLOBALS',
    '_mwiVarsCore',
    '_mwiVarsAfter',
    '_mwiDefinesCore',
    '_mwiDefinesAfter',
    'variables',
    'localconfig',
];

function get_defined_globals() {
    global $EXCLUDED_KEYS;
    $o = [];
    foreach($GLOBALS as $k => $v) {
        if(in_array($k, $EXCLUDED_KEYS)) continue;
        $o[$k]=$v;
    }
    return $o;
}

$IP = dirname(realpath($argv[1]));
$_mwiSettingsFile=realpath($argv[1]);
touch($argv[2]);
$_mwiSettingsJSON=realpath($argv[2]);
echo("Saving $_mwiSettingsFile to {$argv[2]}...\n");
chdir(dirname($_mwiSettingsFile));

$cfgfile = file_get_contents($_mwiSettingsFile);
$cfgfile = preg_replace('/(require|include|require_once)\s+("[^"]+");/', 'mwiRequireOnce($2);', $cfgfile);
$cfgfile = preg_replace('/(require|include|require_once)\(("[^"]+")\);/', 'mwiRequireOnce($2);', $cfgfile);
#$cfgfile = str_replace("require_once", 'mwiRequireOnce', $cfgfile);
#$cfgfile = str_replace("require", 'mwiRequireOnce', $cfgfile);
file_put_contents('LocalSettings.tmp.php', $cfgfile);
foreach($mwDefines as $k=>$v) {
    define($k, $v);
}
foreach($mwVars as $k=>$v){
    $GLOBALS[$k]=$v;
}
$oldglobals = get_defined_globals();
require('LocalSettings.tmp.php');
unset($cfgfile);

$cfg = [];
foreach (get_defined_globals() as $k => $v) {
    switch($k) {
        #case "_mwiExtensions": continue 2;
        #case "_mwiIncludes": continue 2;
        #case "_mwiSkins": continue 2;
        case "_mwiSettingsFile": continue 2;
        case "_mwiSettingsJSON": continue 2;

        case "cfg": continue 2;
        case "k": continue 2;
        case "v": continue 2;
        case "IP": continue 2;
        case "cfgfile": continue 2;
        case "oldglobals": continue 2;
        case "mwDefines": continue 2;
        case "mwVars": continue 2;
        case "wgConf": continue 2; # Uses setstate
        case "wgJobClasses": continue 2; # Uses Closure

        # PHP superglobals
        case "_POST": continue 2;
        case "_SERVER": continue 2;
        case "_COOKIE": continue 2;
        case "_FILES": continue 2;
        case "_GET": continue 2;
        case "argc": continue 2;
        case "argv": continue 2;
        case "GLOBALS": continue 2;

        case "variables": continue 2;
    }
    #var_dump($k);
    #var_dump($v);
    if (is_object($v)) {
        echo("Skipping $k: object\n");
        continue;
    }
    if (is_resource($v)) {
        echo("Skipping $k: resource\n");
        continue;
    }
    if (is_callable($v)) {
        echo("Skipping $k: callable\n");
        continue;
    }
    if(!isset($oldglobals[$k]) or $v != $oldglobals[$k])
        $cfg[$k]=$v;
}
echo("Saving {$_mwiSettingsJSON}...\n");
$bytes = file_put_contents($_mwiSettingsJSON, json_encode($cfg, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT));
echo("  {$bytes}B written.\n");

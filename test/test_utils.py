import unittest

from mwi.utils import merge_dict, diff_dict

class MergeDictTests(unittest.TestCase):

    def test_merge_dict_smoke_001(self):
        source = {
            'str': 'a',
            'dict': {'whoa': 'cool'}
        }
        dest = {
            'str': 'b',
            'int': 1,
            'bool': True,
            'float': 0.1,
            'list': [1, 2, 3],
            'dict': {'nice': 'neat'}
        }
        expected = {
            'str': 'a',
            'int': 1,
            'bool': True,
            'float': 0.1,
            'list': [1, 2, 3],
            'dict': {
                'nice': 'neat',
                'whoa': 'cool'
            }
        }
        self.assertDictEqual(expected, merge_dict(source, dest))

class DiffDictTests(unittest.TestCase):
    def test_php_get_dict_sets_smoke_001(self):
        a = {
            'str': 'a',
            'int': 1,
            'bool': True,
            'float': 0.1,
            'list': [1, 2, 4],
            'dict': {
                'nice': 'neat',
                'deleted': 'oh no',
                '3d-level': {
                    'a': "1",
                    'b': "2"
                }
            }
        }
        b = {
            'str': 'b',
            'int': 1,
            'bool': True,
            'float': 0.1,
            'list': [1, 2, 3],
            'dict': {
                'nice': 'neat',
                'added': 'sweet',
                '3d-level': {
                    'a': "1",
                    'b': "2",
                    'c': "3",
                }
            }
        }
        expected = {
            'str': 'b',
            'list': [1,2,3],
            'dict': {'added': 'sweet', '3d-level':{'c':'3'}}
        }
        actual = diff_dict(a,b)
        self.assertDictEqual(expected, actual)


if __name__ == '__main__':
    import logging
    from buildtools import log
    log.log.setLevel(logging.WARNING)
    unittest.main()

import unittest

from test.localsettings.parser.tester import get_test_data_suite as all_localsettings_parser_tests
from test.localsettings.writer.tester import get_test_data_suite as all_localsettings_writer_tests

runner = unittest.TextTestRunner()
runner.run(all_localsettings_parser_tests())
runner.run(all_localsettings_writer_tests())

import os, json, unittest, sys
from buildtools import os_utils, log
from buildtools.config import YAMLConfig

PHP = os_utils.assertWhich('PHP')

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
MWI_DIR = os.path.dirname(os.path.dirname(os.path.dirname(SCRIPT_DIR)))

sys.path.insert(0, os.path.join(MWI_DIR, 'src', 'python'))

from mediawiki.config import MWConfig

TEST_CASES = [
    'ancient'
]

class TestLocalSettingsWriter(unittest.TestCase):
    def __init__(self, casesubdir):
        unittest.TestCase.__init__(self, methodName='test_parser')
        self.casesubdir = casesubdir

    def test_parser(self):
        with os_utils.Chdir(MWI_DIR):
            inputfile = os.path.join(SCRIPT_DIR, self.casesubdir, 'mwi-config.yml')
            actualfile = os.path.join(SCRIPT_DIR, self.casesubdir, 'LocalSettings.actual.php')
            expectedfile = os.path.join(SCRIPT_DIR, self.casesubdir, 'LocalSettings.expected.php')

            globalcfg = YAMLConfig()
            globalcfg.set('filesystem.pathtemplate', os.path.join(SCRIPT_DIR, self.casesubdir))
            localsettings = MWConfig('example.io', globalcfg, MWI_DIR, write_only=True)
            localsettings.saveLocalSettings(actualfile)

            expected=[]
            with open(expectedfile, 'r') as f:
                expected = f.read().split('\n')
            actual=[]
            with open(actualfile, 'r') as f:
                actual = f.read().split('\n')

            self.assertSetEqual(set(expected), set(actual))

    def shortDescription(self):
        # We need to distinguish between instances of this test case.
        return f'LocalSettings writer case {self.casesubdir}'

def load_tests(loader, tests, pattern):
    return unittest.TestSuite([TestLocalSettingsWriter(n) for n in TEST_CASES])

if __name__ == '__main__':
    import logging
    log.log.setLevel(logging.WARNING)
    unittest.main()

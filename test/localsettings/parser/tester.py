import os, json, unittest
from buildtools import os_utils
PHP = os_utils.assertWhich('php')

SCRIPT_DIR = os.path.dirname(os.path.abspath('__file__'))
MWI_DIR = os.path.dirname(os.path.dirname(os.path.dirname(SCRIPT_DIR)))

TEST_CASES = [
    'ancient'
]

class TestLocalSettingsParser(unittest.TestCase):
    def __init__(self, casesubdir):
        unittest.TestCase.__init__(self, methodName='test_parser')
        self.casesubdir = casesubdir

    def test_parser(self):
        with os_utils.Chdir(MWI_DIR):
            inputfile = os.path.join(SCRIPT_DIR, self.casesubdir, 'LocalSettings.php')
            actualfile = os.path.join(SCRIPT_DIR, self.casesubdir, 'variables.actual.json')
            expectedfile = os.path.join(SCRIPT_DIR, self.casesubdir, 'variables.expected.json')

            os_utils.cmd([PHP, os.path.join(MWI_DIR, 'vars2json.php'), inputfile, actualfile], show_output=False, echo=False, critical=True)

            expected={}
            with open(expectedfile, 'r') as f:
                expected = json.load(f)
            actual={}
            with open(actualfile, 'r') as f:
                actual = json.load(f)

            self.assertEqual(expected, actual)

    def shortDescription(self):
        # We need to distinguish between instances of this test case.
        return f'LocalSettings {self.casesubdir} case'


def get_test_data_suite():
    return unittest.TestSuite([TestLocalSettingsParser(n) for n in TEST_CASES])

if __name__ == '__main__':
    testRunner = unittest.TextTestRunner()
    testRunner.run(get_test_data_suite())

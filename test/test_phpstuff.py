import unittest

from mwi.phpstuff import php_gen_dict_sets

class PHPGenDictSetsTests(unittest.TestCase):
    def test_php_get_dict_sets_smoke_001(self):
        a = {
            'str': 'a',
            'int': 1,
            'bool': True,
            'float': 0.1,
            'list': [1, 2, 4],
            'dict': {
                'nice': 'neat',
                'deleted': 'oh no'
            }
        }
        b = {
            'str': 'b',
            'int': 1,
            'bool': True,
            'float': 0.1,
            'list': [1, 2, 3],
            'dict': {'nice': 'neat',
            'added': 'sweet'}
        }
        expected = set([
            "$a['str'] = 'b';",
            "if (\\in_array(4, $a['list'])) $a['list'] = \\array_splice($a['list'], array_search(4, $a['list'], true), 1);",
            "$a['list'][] = 3;",
            "delete $a['dict']['deleted'];",
            "$a['dict']['added'] = 'sweet';",
        ])
        self.assertSetEqual(expected, set(php_gen_dict_sets('a',a,b)))


if __name__ == '__main__':
    import logging
    from buildtools import log
    log.log.setLevel(logging.WARNING)
    unittest.main()
